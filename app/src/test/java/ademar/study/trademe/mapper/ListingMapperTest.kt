package ademar.study.trademe.mapper

import ademar.study.trademe.test.BaseTest
import ademar.study.trademe.test.Fixture.LISTING_DETAIL_TITLE
import ademar.study.trademe.test.Fixture.PHOTO_VALUE_FULL_SIZE
import ademar.study.trademe.test.Fixture.PHOTO_VALUE_GALLERY
import ademar.study.trademe.test.Fixture.PHOTO_VALUE_LARGE
import ademar.study.trademe.test.Fixture.PHOTO_VALUE_LIST
import ademar.study.trademe.test.Fixture.PHOTO_VALUE_MEDIUM
import ademar.study.trademe.test.Fixture.PHOTO_VALUE_THUMBNAIL
import ademar.study.trademe.test.Fixture.listingDetail
import ademar.study.trademe.test.Fixture.photo
import ademar.study.trademe.test.Fixture.photoValue
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class ListingMapperTest : BaseTest() {

    @Test
    fun testTransform() {
        val mapper = ListingMapper()
        val viewModel = mapper.transform(listingDetail())

        assertThat(viewModel.name).isEqualTo(LISTING_DETAIL_TITLE)
        assertThat(viewModel.photos).isEqualTo(listOf(PHOTO_VALUE_FULL_SIZE))
    }

    @Test
    fun testTransform_photos() {
        val mapper = ListingMapper()
        val viewModel = mapper.transform(listingDetail().copy(photos = listOf(photo(), photo())))

        assertThat(viewModel.name).isEqualTo(LISTING_DETAIL_TITLE)
        assertThat(viewModel.photos).isEqualTo(listOf(PHOTO_VALUE_FULL_SIZE, PHOTO_VALUE_FULL_SIZE))
    }

    @Test
    fun testTransform_noPhotos() {
        val mapper = ListingMapper()
        val viewModel = mapper.transform(listingDetail().copy(photos = listOf()))

        assertThat(viewModel.name).isEqualTo(LISTING_DETAIL_TITLE)
        assertThat(viewModel.photos).isEqualTo(listOf<String>())
    }

    @Test
    fun testTransform_large() {
        val mapper = ListingMapper()
        val viewModel = mapper.transform(listingDetail().copy(photos = listOf(photo().copy(photoValue().copy(
                fullSize = ""
        )))))

        assertThat(viewModel.name).isEqualTo(LISTING_DETAIL_TITLE)
        assertThat(viewModel.photos).isEqualTo(listOf(PHOTO_VALUE_LARGE))
    }

    @Test
    fun testTransform_gallery() {
        val mapper = ListingMapper()
        val viewModel = mapper.transform(listingDetail().copy(photos = listOf(photo().copy(photoValue().copy(
                fullSize = "",
                large = ""
        )))))

        assertThat(viewModel.name).isEqualTo(LISTING_DETAIL_TITLE)
        assertThat(viewModel.photos).isEqualTo(listOf(PHOTO_VALUE_GALLERY))
    }

    @Test
    fun testTransform_medium() {
        val mapper = ListingMapper()
        val viewModel = mapper.transform(listingDetail().copy(photos = listOf(photo().copy(photoValue().copy(
                fullSize = "",
                large = "",
                gallery = ""
        )))))

        assertThat(viewModel.name).isEqualTo(LISTING_DETAIL_TITLE)
        assertThat(viewModel.photos).isEqualTo(listOf(PHOTO_VALUE_MEDIUM))
    }

    @Test
    fun testTransform_list() {
        val mapper = ListingMapper()
        val viewModel = mapper.transform(listingDetail().copy(photos = listOf(photo().copy(photoValue().copy(
                fullSize = "",
                large = "",
                gallery = "",
                medium = ""
        )))))

        assertThat(viewModel.name).isEqualTo(LISTING_DETAIL_TITLE)
        assertThat(viewModel.photos).isEqualTo(listOf(PHOTO_VALUE_LIST))
    }

    @Test
    fun testTransform_thumbnail() {
        val mapper = ListingMapper()
        val viewModel = mapper.transform(listingDetail().copy(photos = listOf(photo().copy(photoValue().copy(
                fullSize = "",
                large = "",
                gallery = "",
                medium = "",
                list = ""
        )))))

        assertThat(viewModel.name).isEqualTo(LISTING_DETAIL_TITLE)
        assertThat(viewModel.photos).isEqualTo(listOf(PHOTO_VALUE_THUMBNAIL))
    }

    @Test
    fun testTransform_none() {
        val mapper = ListingMapper()
        val viewModel = mapper.transform(listingDetail().copy(photos = listOf(photo().copy(photoValue().copy(
                fullSize = "",
                large = "",
                gallery = "",
                medium = "",
                list = "",
                thumbnail = ""
        )))))

        assertThat(viewModel.name).isEqualTo(LISTING_DETAIL_TITLE)
        assertThat(viewModel.photos).isEqualTo(listOf<String>())
    }

}
