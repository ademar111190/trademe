package ademar.study.trademe.mapper

import ademar.study.trademe.R
import ademar.study.trademe.test.BaseTest
import ademar.study.trademe.test.Fixture.SEARCH_RESULT_MESSAGE
import ademar.study.trademe.test.Fixture.SEARCH_RESULT_MESSAGE_EMPTY
import ademar.study.trademe.test.Fixture.SEARCH_RESULT_MESSAGE_MULTIPLE
import ademar.study.trademe.test.Fixture.listing
import com.nhaarman.mockito_kotlin.whenever
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test

class SearchResultMapperTest : BaseTest() {

    @Before
    override fun setUp() {
        super.setUp()
        whenever(mockResources.getQuantityString(R.plurals.search_result, 0)).thenReturn(SEARCH_RESULT_MESSAGE_EMPTY)
        whenever(mockResources.getQuantityString(R.plurals.search_result, 1)).thenReturn(SEARCH_RESULT_MESSAGE)
        whenever(mockResources.getQuantityString(R.plurals.search_result, 2)).thenReturn(SEARCH_RESULT_MESSAGE_MULTIPLE)
    }

    @Test
    fun testTransform_empty() {
        val mapper = SearchResultMapper(mockContext)
        val viewModel = mapper.transform(listOf())

        assertThat(viewModel.message).isEqualTo(SEARCH_RESULT_MESSAGE_EMPTY)
        assertThat(viewModel.listings).isEmpty()
    }

    @Test
    fun testTransform_one() {
        val mapper = SearchResultMapper(mockContext)
        val viewModel = mapper.transform(listOf(listing()))

        assertThat(viewModel.message).isEqualTo(SEARCH_RESULT_MESSAGE)
        assertThat(viewModel.listings).isEqualTo(listOf(listing()))
    }

    @Test
    fun testTransform_multiple() {
        val mapper = SearchResultMapper(mockContext)
        val viewModel = mapper.transform(listOf(listing(), listing()))

        assertThat(viewModel.message).isEqualTo(SEARCH_RESULT_MESSAGE_MULTIPLE)
        assertThat(viewModel.listings).isEqualTo(listOf(listing(), listing()))
    }

}
