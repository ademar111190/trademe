package ademar.study.trademe.mapper

import ademar.study.trademe.test.BaseTest
import ademar.study.trademe.test.Fixture.CATEGORY_COUNT
import ademar.study.trademe.test.Fixture.CATEGORY_NAME
import ademar.study.trademe.test.Fixture.category
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class CategoryMapperTest : BaseTest() {

    @Test
    fun testTransform() {
        val mapper = CategoryMapper()
        val viewModel = mapper.transform(category())
        assertThat(viewModel.title).isEqualTo(CATEGORY_NAME)
        assertThat(viewModel.amount).isEqualTo("($CATEGORY_COUNT)")
    }

}
