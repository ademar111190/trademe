package ademar.study.trademe.mapper

import ademar.study.trademe.R
import ademar.study.trademe.test.BaseTest
import ademar.study.trademe.test.Fixture.CATEGORY_NAME
import ademar.study.trademe.test.Fixture.SEARCH_INTRO_MESSAGE
import ademar.study.trademe.test.Fixture.SEARCH_INTRO_MESSAGE_CATEGORY
import ademar.study.trademe.test.Fixture.category
import com.nhaarman.mockito_kotlin.whenever
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class SearchIntroMapperTest : BaseTest() {

    @Test
    fun testTransform_category() {
        whenever(mockContext.getString(R.string.search_intro_catalog, CATEGORY_NAME)).thenReturn(SEARCH_INTRO_MESSAGE_CATEGORY)

        val mapper = SearchIntroMapper(mockContext)
        val viewModel = mapper.transform(category())

        assertThat(viewModel.message).isEqualTo(SEARCH_INTRO_MESSAGE_CATEGORY)
    }

    @Test
    fun testTransform_noCategory() {
        whenever(mockContext.getString(R.string.search_intro)).thenReturn(SEARCH_INTRO_MESSAGE)

        val mapper = SearchIntroMapper(mockContext)
        val viewModel = mapper.transform(null)

        assertThat(viewModel.message).isEqualTo(SEARCH_INTRO_MESSAGE)
    }

}
