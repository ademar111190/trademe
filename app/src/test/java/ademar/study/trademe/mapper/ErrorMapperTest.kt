package ademar.study.trademe.mapper

import ademar.study.trademe.test.BaseTest
import ademar.study.trademe.test.Fixture.MESSAGE
import ademar.study.trademe.test.Fixture.ERROR_NO_CONNECTION
import ademar.study.trademe.test.Fixture.ERROR_UNAUTHORIZED
import ademar.study.trademe.test.Fixture.ERROR_UNKNOWN
import com.nhaarman.mockito_kotlin.whenever
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mockito.Mock
import retrofit2.HttpException
import retrofit2.Response
import java.net.UnknownHostException

class ErrorMapperTest : BaseTest() {

    @Mock lateinit var mockResponse: Response<Any>

    @Test
    fun testTransform_null() {
        val mapper = ErrorMapper(mockContext)
        val viewModel = mapper.transform(null)
        assertThat(viewModel.message).isEqualTo(ERROR_UNKNOWN)
    }

    @Test
    fun testTransform_noConnection() {
        val mapper = ErrorMapper(mockContext)
        val viewModel = mapper.transform(UnknownHostException())
        assertThat(viewModel.message).isEqualTo(ERROR_NO_CONNECTION)
    }

    @Test
    fun testTransform_401() {
        whenever(mockResponse.code()).thenReturn(401)
        val mapper = ErrorMapper(mockContext)
        val viewModel = mapper.transform(HttpException(mockResponse))
        assertThat(viewModel.message).isEqualTo(ERROR_UNAUTHORIZED)
    }

    @Test
    fun testTransform_otherCode() {
        whenever(mockResponse.message()).thenReturn(MESSAGE)
        val mapper = ErrorMapper(mockContext)
        val viewModel = mapper.transform(HttpException(mockResponse))
        assertThat(viewModel.message).isEqualTo(MESSAGE)
    }

    @Test
    fun testTransform_otherCode_noMessage() {
        val mapper = ErrorMapper(mockContext)
        val viewModel = mapper.transform(HttpException(mockResponse))
        assertThat(viewModel.message).isEqualTo(ERROR_UNKNOWN)
    }

    @Test
    fun testTransform_otherError() {
        val mapper = ErrorMapper(mockContext)
        val viewModel = mapper.transform(Error(MESSAGE))
        assertThat(viewModel.message).isEqualTo(MESSAGE)
    }

    @Test
    fun testTransform_otherError_noMessage() {
        val mapper = ErrorMapper(mockContext)
        val viewModel = mapper.transform(Error())
        assertThat(viewModel.message).isEqualTo(ERROR_UNKNOWN)
    }

}
