package ademar.study.trademe.navigation

import ademar.study.trademe.R
import ademar.study.trademe.test.BaseTest
import ademar.study.trademe.test.Fixture.CATEGORY_NAME
import ademar.study.trademe.test.Fixture.category
import ademar.study.trademe.view.base.BaseActivity
import ademar.study.trademe.view.category.CategoryActivity
import ademar.study.trademe.view.category.CategoryFragment
import ademar.study.trademe.view.common.WelcomeFragment
import ademar.study.trademe.view.detail.DetailActivity
import ademar.study.trademe.view.detail.DetailFragment
import ademar.study.trademe.view.search.SearchActivity
import ademar.study.trademe.view.search.SearchFragment
import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import androidx.core.os.bundleOf
import com.nhaarman.mockito_kotlin.whenever
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*

class FlowControllerTest : BaseTest() {

    @Mock lateinit var mockBaseActivity: BaseActivity
    @Mock lateinit var mockDetailActivity: DetailActivity
    @Mock lateinit var mockSearchActivity: SearchActivity
    @Mock lateinit var mockCategoryActivity: CategoryActivity
    @Mock lateinit var mockFragmentManager: FragmentManager
    @Mock lateinit var mockFragmentTransaction: FragmentTransaction
    @Mock lateinit var mockDetailActivityIntent: Intent
    @Mock lateinit var mockSearchActivityIntent: Intent
    @Mock lateinit var mockCategoryActivityIntent: Intent
    @Mock lateinit var mockIntent: Intent
    @Mock lateinit var mockIntentFactory: IntentFactory

    @SuppressLint("CommitTransaction")
    @Before
    override fun setUp() {
        super.setUp()
        whenever(mockIntentFactory.makeIntent()).thenReturn(mockIntent)
        whenever(mockBaseActivity.resources).thenReturn(mockResources)
        whenever(mockBaseActivity.supportFragmentManager).thenReturn(mockFragmentManager)
        whenever(mockFragmentManager.beginTransaction()).thenReturn(mockFragmentTransaction)
    }

    @Test
    fun testLaunchHome() {
        val flowController = FlowController(mockBaseActivity, mockIntentFactory)

        flowController.launchHome()

        verify(mockIntentFactory).makeIntent()
        verifyNoMoreInteractions(mockIntentFactory)
        verify(mockIntent).setClassName(mockBaseActivity, CategoryActivity::class.java.name)
        verifyNoMoreInteractions(mockIntent)
        verify(mockBaseActivity).startActivity(mockIntent)
        verifyNoMoreInteractions(mockBaseActivity)
    }

    @Test
    fun testLaunchCategory_smallScreen() {
        whenever(mockResources.getBoolean(R.bool.large_screen)).thenReturn(false)

        val flowController = FlowController(mockBaseActivity, mockIntentFactory)

        flowController.launchCategory(category())

        verify(mockIntentFactory).makeIntent()
        verifyNoMoreInteractions(mockIntentFactory)
        verify(mockIntent).setClassName(mockBaseActivity, CategoryActivity::class.java.name)
        verify(mockIntent).putExtras(any<Bundle>())
        verifyNoMoreInteractions(mockIntent)
        verify(mockBaseActivity).startActivity(mockIntent)
        verify(mockBaseActivity).resources
        verifyNoMoreInteractions(mockBaseActivity)
    }

    @Test
    fun testLaunchCategory_largeScreen() {
        whenever(mockResources.getBoolean(R.bool.large_screen)).thenReturn(true)
        whenever(mockFragmentTransaction.replace(eq(R.id.category_fragment), any(CategoryFragment::class.java))).thenReturn(mockFragmentTransaction)
        whenever(mockFragmentTransaction.addToBackStack(CATEGORY_NAME)).thenReturn(mockFragmentTransaction)

        val flowController = FlowController(mockBaseActivity, mockIntentFactory)

        flowController.launchCategory(category())

        verify(mockFragmentTransaction).replace(eq(R.id.category_fragment), any(CategoryFragment::class.java))
        verify(mockFragmentTransaction).addToBackStack(CATEGORY_NAME)
        verify(mockFragmentTransaction).commit()
        verifyNoMoreInteractions(mockFragmentTransaction)
    }

    @Test
    fun testLaunchCategory_fromCategoryActivity() {
        whenever(mockCategoryActivity.intent).thenReturn(mockCategoryActivityIntent)
        whenever(mockCategoryActivityIntent.extras).thenReturn(bundleOf())

        whenever(mockResources.getBoolean(R.bool.large_screen)).thenReturn(true)
        whenever(mockFragmentTransaction.replace(eq(R.id.category_fragment), any(CategoryFragment::class.java))).thenReturn(mockFragmentTransaction)
        whenever(mockFragmentTransaction.replace(eq(R.id.detail_fragment), any(WelcomeFragment::class.java))).thenReturn(mockFragmentTransaction)

        val flowController = FlowController(mockBaseActivity, mockIntentFactory)

        flowController.launchCategory(mockCategoryActivity)

        verify(mockFragmentTransaction).replace(eq(R.id.category_fragment), any(CategoryFragment::class.java))
        verify(mockFragmentTransaction).replace(eq(R.id.detail_fragment), any(WelcomeFragment::class.java))
        verify(mockFragmentTransaction, times(2)).commit()
        verifyNoMoreInteractions(mockFragmentTransaction)
    }

    @Test
    fun testLaunchDetail_smallScreen() {
        whenever(mockResources.getBoolean(R.bool.large_screen)).thenReturn(false)

        val flowController = FlowController(mockBaseActivity, mockIntentFactory)

        flowController.launchDetail(category())

        verify(mockIntentFactory).makeIntent()
        verifyNoMoreInteractions(mockIntentFactory)
        verify(mockIntent).setClassName(mockBaseActivity, DetailActivity::class.java.name)
        verify(mockIntent).putExtras(any<Bundle>())
        verifyNoMoreInteractions(mockIntent)
        verify(mockBaseActivity).startActivity(mockIntent)
        verify(mockBaseActivity).resources
        verifyNoMoreInteractions(mockBaseActivity)
    }

    @Test
    fun testLaunchDetail_largeScreen() {
        whenever(mockResources.getBoolean(R.bool.large_screen)).thenReturn(true)
        whenever(mockFragmentTransaction.replace(eq(R.id.detail_fragment), any(DetailFragment::class.java))).thenReturn(mockFragmentTransaction)

        val flowController = FlowController(mockBaseActivity, mockIntentFactory)

        flowController.launchDetail(category())

        verify(mockFragmentTransaction).replace(eq(R.id.detail_fragment), any(DetailFragment::class.java))
        verify(mockFragmentTransaction).commit()
        verifyNoMoreInteractions(mockFragmentTransaction)
    }

    @Test
    fun testLaunchDetail_fromDetailActivity() {
        whenever(mockDetailActivity.intent).thenReturn(mockDetailActivityIntent)
        whenever(mockDetailActivityIntent.extras).thenReturn(bundleOf())

        whenever(mockResources.getBoolean(R.bool.large_screen)).thenReturn(true)
        whenever(mockFragmentTransaction.replace(eq(R.id.detail_fragment), any(DetailFragment::class.java))).thenReturn(mockFragmentTransaction)

        val flowController = FlowController(mockBaseActivity, mockIntentFactory)

        flowController.launchDetail(mockDetailActivity)

        verify(mockFragmentTransaction).replace(eq(R.id.detail_fragment), any(DetailFragment::class.java))
        verify(mockFragmentTransaction).commit()
        verifyNoMoreInteractions(mockFragmentTransaction)
    }

    @Test
    fun testLaunchSearch() {
        val flowController = FlowController(mockBaseActivity, mockIntentFactory)

        flowController.launchSearch(category())

        verify(mockIntentFactory).makeIntent()
        verifyNoMoreInteractions(mockIntentFactory)
        verify(mockIntent).setClassName(mockBaseActivity, SearchActivity::class.java.name)
        verify(mockIntent).putExtras(any<Bundle>())
        verifyNoMoreInteractions(mockIntent)
        verify(mockBaseActivity).startActivity(mockIntent)
        verifyNoMoreInteractions(mockBaseActivity)
    }

    @Test
    fun testLaunchSearch_fromSearchActivity() {
        whenever(mockSearchActivity.intent).thenReturn(mockSearchActivityIntent)
        whenever(mockSearchActivityIntent.extras).thenReturn(bundleOf())

        whenever(mockFragmentTransaction.replace(eq(R.id.search_fragment), any(SearchFragment::class.java))).thenReturn(mockFragmentTransaction)

        val flowController = FlowController(mockBaseActivity, mockIntentFactory)

        flowController.launchSearch(mockSearchActivity)

        verify(mockFragmentTransaction).replace(eq(R.id.search_fragment), any(SearchFragment::class.java))
        verify(mockFragmentTransaction).commit()
        verifyNoMoreInteractions(mockFragmentTransaction)
    }

}
