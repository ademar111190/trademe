package ademar.study.trademe.presenter.detail

import ademar.study.trademe.core.model.Listing
import ademar.study.trademe.presenter.StubLoadDataView
import org.assertj.core.api.Assertions.fail

open class StubDetailView : StubLoadDataView(), DetailView {

    override fun bindListings(listings: List<Listing>) = fail("Shouldn't call bindListings, listings $listings")

}
