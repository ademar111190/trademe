package ademar.study.trademe.presenter.search

import ademar.study.trademe.core.interactor.SearchListing
import ademar.study.trademe.core.model.Category
import ademar.study.trademe.mapper.ErrorMapper
import ademar.study.trademe.mapper.SearchIntroMapper
import ademar.study.trademe.mapper.SearchResultMapper
import ademar.study.trademe.model.ErrorViewModel
import ademar.study.trademe.model.SearchIntroViewModel
import ademar.study.trademe.model.SearchResultViewModel
import ademar.study.trademe.test.BaseTest
import ademar.study.trademe.test.Fixture
import ademar.study.trademe.test.Fixture.SEARCH_QUERY
import ademar.study.trademe.test.Fixture.category
import ademar.study.trademe.test.Fixture.errorViewModel
import ademar.study.trademe.test.Fixture.listing
import ademar.study.trademe.test.Fixture.searchIntroViewModel
import ademar.study.trademe.test.Fixture.searchResultViewModel
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Observable
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

class SearchPresenterTest : BaseTest() {

    @Mock lateinit var mockSearchListing: SearchListing
    @Mock lateinit var mockSearchIntroMapper: SearchIntroMapper
    @Mock lateinit var mockSearchResultMapper: SearchResultMapper
    @Mock lateinit var mockErrorMapper: ErrorMapper

    private lateinit var mockCategory: Category
    private lateinit var mockError: Error
    private lateinit var mockErrorViewModel: ErrorViewModel

    private var bindIntroCount = 0
    private var clearResultCount = 0
    private var showEmptyStateCount = 0
    private var bindResultCount = 0
    private var showContentCount = 0
    private var showErrorCount = 0
    private var showLoadingCount = 0
    private var showRetryCount = 0

    @Before
    override fun setUp() {
        super.setUp()

        mockCategory = category()
        mockError = Fixture.error()
        mockErrorViewModel = errorViewModel()

        whenever(mockSearchIntroMapper.transform(any())).thenReturn(searchIntroViewModel())
        whenever(mockSearchResultMapper.transform(any())).thenReturn(searchResultViewModel())
        whenever(mockErrorMapper.transform(any())).thenReturn(mockErrorViewModel)

        bindIntroCount = 0
        clearResultCount = 0
        showEmptyStateCount = 0
        bindResultCount = 0
        showContentCount = 0
        showErrorCount = 0
        showLoadingCount = 0
        showRetryCount = 0
    }

    @Test
    fun testOnAttachView() {
        val stubView = object : StubSearchView() {}
        val presenter = SearchPresenter(mockSearchListing, mockSearchIntroMapper, mockSearchResultMapper, mockErrorMapper, mockCategory)
        presenter.onAttachView(stubView)
    }

    @Test
    fun testOnStart() {
        val stubView = object : StubSearchView() {
            override fun showLoading() {
                showLoadingCount++
            }

            override fun clearResult() {
                clearResultCount++
            }

            override fun bindIntro(viewModel: SearchIntroViewModel) {
                assertThat(viewModel).isEqualTo(searchIntroViewModel())
                bindIntroCount++
            }

            override fun showEmptyState() {
                showEmptyStateCount++
            }
        }

        val presenter = SearchPresenter(mockSearchListing, mockSearchIntroMapper, mockSearchResultMapper, mockErrorMapper, mockCategory)
        presenter.onAttachView(stubView)
        presenter.onStart()

        assertThat(bindIntroCount).isEqualTo(1)
        assertThat(clearResultCount).isEqualTo(1)
        assertThat(showEmptyStateCount).isEqualTo(1)
        assertThat(bindResultCount).isEqualTo(0)
        assertThat(showContentCount).isEqualTo(0)
        assertThat(showErrorCount).isEqualTo(0)
        assertThat(showLoadingCount).isEqualTo(1)
        assertThat(showRetryCount).isEqualTo(0)
    }

    @Test
    fun testSearch_noQuery() {
        val stubView = object : StubSearchView() {
            override fun showLoading() {
                showLoadingCount++
            }

            override fun clearResult() {
                clearResultCount++
            }

            override fun bindIntro(viewModel: SearchIntroViewModel) {
                assertThat(viewModel).isEqualTo(searchIntroViewModel())
                bindIntroCount++
            }

            override fun showEmptyState() {
                showEmptyStateCount++
            }
        }

        val presenter = SearchPresenter(mockSearchListing, mockSearchIntroMapper, mockSearchResultMapper, mockErrorMapper, mockCategory)
        presenter.onAttachView(stubView)
        presenter.search("")

        assertThat(bindIntroCount).isEqualTo(1)
        assertThat(clearResultCount).isEqualTo(1)
        assertThat(showEmptyStateCount).isEqualTo(1)
        assertThat(bindResultCount).isEqualTo(0)
        assertThat(showContentCount).isEqualTo(0)
        assertThat(showErrorCount).isEqualTo(0)
        assertThat(showLoadingCount).isEqualTo(1)
        assertThat(showRetryCount).isEqualTo(0)
    }

    @Test
    fun testSearch_blankQuery() {
        val stubView = object : StubSearchView() {
            override fun showLoading() {
                showLoadingCount++
            }

            override fun clearResult() {
                clearResultCount++
            }

            override fun bindIntro(viewModel: SearchIntroViewModel) {
                assertThat(viewModel).isEqualTo(searchIntroViewModel())
                bindIntroCount++
            }

            override fun showEmptyState() {
                showEmptyStateCount++
            }
        }

        val presenter = SearchPresenter(mockSearchListing, mockSearchIntroMapper, mockSearchResultMapper, mockErrorMapper, mockCategory)
        presenter.onAttachView(stubView)
        presenter.search("   ")

        assertThat(bindIntroCount).isEqualTo(1)
        assertThat(clearResultCount).isEqualTo(1)
        assertThat(showEmptyStateCount).isEqualTo(1)
        assertThat(bindResultCount).isEqualTo(0)
        assertThat(showContentCount).isEqualTo(0)
        assertThat(showErrorCount).isEqualTo(0)
        assertThat(showLoadingCount).isEqualTo(1)
        assertThat(showRetryCount).isEqualTo(0)
    }

    @Test
    fun testSearch_success() {
        val stubView = object : StubSearchView() {
            override fun showLoading() {
                showLoadingCount++
            }

            override fun clearResult() {
                clearResultCount++
            }

            override fun bindResult(viewModel: SearchResultViewModel) {
                assertThat(viewModel).isEqualTo(searchResultViewModel())
                bindResultCount++
            }

            override fun showContent() {
                showContentCount++
            }
        }

        whenever(mockSearchListing.execute(SEARCH_QUERY, mockCategory)).thenReturn(Observable.just(listing()))

        val presenter = SearchPresenter(mockSearchListing, mockSearchIntroMapper, mockSearchResultMapper, mockErrorMapper, mockCategory)
        presenter.onAttachView(stubView)
        presenter.search(SEARCH_QUERY)

        assertThat(bindIntroCount).isEqualTo(0)
        assertThat(clearResultCount).isEqualTo(1)
        assertThat(showEmptyStateCount).isEqualTo(0)
        assertThat(bindResultCount).isEqualTo(1)
        assertThat(showContentCount).isEqualTo(1)
        assertThat(showErrorCount).isEqualTo(0)
        assertThat(showLoadingCount).isEqualTo(1)
        assertThat(showRetryCount).isEqualTo(0)
    }

    @Test
    fun testSearch_error() {
        val stubView = object : StubSearchView() {
            override fun showLoading() {
                showLoadingCount++
            }

            override fun clearResult() {
                clearResultCount++
            }

            override fun showError(viewModel: ErrorViewModel) {
                assertThat(viewModel).isEqualTo(mockErrorViewModel)
                showErrorCount++
            }

            override fun showRetry() {
                showRetryCount++
            }
        }

        whenever(mockSearchListing.execute(SEARCH_QUERY, mockCategory)).thenReturn(Observable.error(Fixture.error()))

        val presenter = SearchPresenter(mockSearchListing, mockSearchIntroMapper, mockSearchResultMapper, mockErrorMapper, mockCategory)
        presenter.onAttachView(stubView)
        presenter.search(SEARCH_QUERY)

        assertThat(bindIntroCount).isEqualTo(0)
        assertThat(clearResultCount).isEqualTo(1)
        assertThat(showEmptyStateCount).isEqualTo(0)
        assertThat(bindResultCount).isEqualTo(0)
        assertThat(showContentCount).isEqualTo(0)
        assertThat(showErrorCount).isEqualTo(1)
        assertThat(showLoadingCount).isEqualTo(1)
        assertThat(showRetryCount).isEqualTo(1)
    }

    @Test
    fun testOnDetachView() {
        val presenter = SearchPresenter(mockSearchListing, mockSearchIntroMapper, mockSearchResultMapper, mockErrorMapper, mockCategory)
        presenter.onDetachView()
    }

}
