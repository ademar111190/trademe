package ademar.study.trademe.presenter.detail

import ademar.study.trademe.core.interactor.SearchListing
import ademar.study.trademe.core.model.Category
import ademar.study.trademe.core.model.Listing
import ademar.study.trademe.mapper.ErrorMapper
import ademar.study.trademe.model.ErrorViewModel
import ademar.study.trademe.test.BaseTest
import ademar.study.trademe.test.Fixture
import ademar.study.trademe.test.Fixture.category
import ademar.study.trademe.test.Fixture.errorViewModel
import ademar.study.trademe.test.Fixture.listing
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Observable
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

class DetailPresenterTest : BaseTest() {

    @Mock lateinit var mockSearchListing: SearchListing
    @Mock lateinit var mockErrorMapper: ErrorMapper

    private lateinit var mockCategory: Category
    private lateinit var mockError: Error
    private lateinit var mockErrorViewModel: ErrorViewModel

    private var bindListingCount = 0
    private var showContentCount = 0
    private var showErrorCount = 0
    private var showLoadingCount = 0
    private var showRetryCount = 0

    @Before
    override fun setUp() {
        super.setUp()

        mockCategory = category()
        mockError = Fixture.error()
        mockErrorViewModel = errorViewModel()

        whenever(mockErrorMapper.transform(any())).thenReturn(mockErrorViewModel)

        bindListingCount = 0
        showContentCount = 0
        showErrorCount = 0
        showLoadingCount = 0
        showRetryCount = 0
    }

    @Test
    fun testOnAttachView() {
        val stubView = object : StubDetailView() {}
        val presenter = DetailPresenter(mockContext, mockSearchListing, mockErrorMapper, mockCategory)
        presenter.onAttachView(stubView)
    }

    @Test
    fun testOnStart_success() {
        val stubView = object : StubDetailView() {
            override fun showLoading() {
                showLoadingCount++
            }

            override fun bindListings(listings: List<Listing>) {
                assertThat(listings).isEqualTo(listOf(listing()))
                bindListingCount++
            }

            override fun showContent() {
                showContentCount++
            }
        }

        whenever(mockSearchListing.execute(category = category())).thenReturn(Observable.just(listing()))

        val presenter = DetailPresenter(mockContext, mockSearchListing, mockErrorMapper, mockCategory)
        presenter.onAttachView(stubView)
        presenter.onStart()

        assertThat(bindListingCount).isEqualTo(1)
        assertThat(showContentCount).isEqualTo(1)
        assertThat(showErrorCount).isEqualTo(0)
        assertThat(showLoadingCount).isEqualTo(1)
        assertThat(showRetryCount).isEqualTo(0)
    }

    @Test
    fun testOnStart_empty() {
        val stubView = object : StubDetailView() {
            override fun showLoading() {
                showLoadingCount++
            }

            override fun showError(viewModel: ErrorViewModel) {
                assertThat(viewModel).isEqualTo(mockErrorViewModel)
                showErrorCount++
            }

            override fun showContent() {
                showContentCount++
            }

            override fun showRetry() {
                showRetryCount++
            }
        }

        whenever(mockSearchListing.execute(category = category())).thenReturn(Observable.empty())

        val presenter = DetailPresenter(mockContext, mockSearchListing, mockErrorMapper, mockCategory)
        presenter.onAttachView(stubView)
        presenter.onStart()

        assertThat(bindListingCount).isEqualTo(0)
        assertThat(showContentCount).isEqualTo(0)
        assertThat(showErrorCount).isEqualTo(1)
        assertThat(showLoadingCount).isEqualTo(1)
        assertThat(showRetryCount).isEqualTo(1)
    }

    @Test
    fun testOnStart_noCategory() {
        val stubView = object : StubDetailView() {
            override fun showLoading() {
                showLoadingCount++
            }

            override fun showError(viewModel: ErrorViewModel) {
                assertThat(viewModel).isEqualTo(mockErrorViewModel)
                showErrorCount++
            }

            override fun showContent() {
                showContentCount++
            }

            override fun showRetry() {
                showRetryCount++
            }
        }

        val presenter = DetailPresenter(mockContext, mockSearchListing, mockErrorMapper, null)
        presenter.onAttachView(stubView)
        presenter.onStart()

        assertThat(bindListingCount).isEqualTo(0)
        assertThat(showContentCount).isEqualTo(0)
        assertThat(showErrorCount).isEqualTo(1)
        assertThat(showLoadingCount).isEqualTo(1)
        assertThat(showRetryCount).isEqualTo(1)
    }

    @Test
    fun testOnStart_error() {
        val stubView = object : StubDetailView() {
            override fun showLoading() {
                showLoadingCount++
            }

            override fun showError(viewModel: ErrorViewModel) {
                assertThat(viewModel).isEqualTo(mockErrorViewModel)
                showErrorCount++
            }

            override fun showRetry() {
                showRetryCount++
            }
        }

        whenever(mockSearchListing.execute(category = category())).thenReturn(Observable.error(Fixture.error()))

        val presenter = DetailPresenter(mockContext, mockSearchListing, mockErrorMapper, mockCategory)
        presenter.onAttachView(stubView)
        presenter.onStart()

        assertThat(bindListingCount).isEqualTo(0)
        assertThat(showContentCount).isEqualTo(0)
        assertThat(showErrorCount).isEqualTo(1)
        assertThat(showLoadingCount).isEqualTo(1)
        assertThat(showRetryCount).isEqualTo(1)
    }

    @Test
    fun testOnDetachView() {
        val presenter = DetailPresenter(mockContext, mockSearchListing, mockErrorMapper, mockCategory)
        presenter.onDetachView()
    }

}
