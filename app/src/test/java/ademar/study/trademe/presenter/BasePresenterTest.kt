package ademar.study.trademe.presenter

import ademar.study.trademe.test.BaseTest
import org.junit.Test

class BasePresenterTest : BaseTest() {

    @Test
    fun testOnAttachView() {
        val stubView = object : StubLoadDataView() {}
        val presenter = BasePresenter<StubLoadDataView>()
        presenter.onAttachView(stubView)
    }

    @Test
    fun testOnDetachView() {
        val presenter = BasePresenter<StubLoadDataView>()
        presenter.onDetachView()
    }

}
