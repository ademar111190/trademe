package ademar.study.trademe.presenter.category

import ademar.study.trademe.core.interactor.GetCategory
import ademar.study.trademe.core.model.Category
import ademar.study.trademe.mapper.CategoryMapper
import ademar.study.trademe.mapper.ErrorMapper
import ademar.study.trademe.model.CategoryViewModel
import ademar.study.trademe.model.ErrorViewModel
import ademar.study.trademe.navigation.FlowController
import ademar.study.trademe.test.BaseTest
import ademar.study.trademe.test.Fixture
import ademar.study.trademe.test.Fixture.CATEGORY_NUMBER
import ademar.study.trademe.test.Fixture.category
import ademar.study.trademe.test.Fixture.categoryViewModel
import ademar.study.trademe.test.Fixture.errorViewModel
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Observable
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions

class CategoryPresenterTest : BaseTest() {

    @Mock lateinit var mockFlowController: FlowController
    @Mock lateinit var mockGetCategory: GetCategory
    @Mock lateinit var mockCategoryMapper: CategoryMapper
    @Mock lateinit var mockErrorMapper: ErrorMapper

    private lateinit var mockCategory: Category
    private lateinit var mockError: Error
    private lateinit var mockCategoryViewModel: CategoryViewModel
    private lateinit var mockErrorViewModel: ErrorViewModel

    private var bindRootCount = 0
    private var clearCategoriesCount = 0
    private var bindCategoryCount = 0
    private var showContentCount = 0
    private var showErrorCount = 0
    private var showLoadingCount = 0
    private var showRetryCount = 0

    @Before
    override fun setUp() {
        super.setUp()

        mockCategory = category()
        mockError = Fixture.error()
        mockCategoryViewModel = categoryViewModel()
        mockErrorViewModel = errorViewModel()

        whenever(mockCategoryMapper.transform(any())).thenReturn(mockCategoryViewModel)
        whenever(mockErrorMapper.transform(any())).thenReturn(mockErrorViewModel)

        bindRootCount = 0
        clearCategoriesCount = 0
        bindCategoryCount = 0
        showContentCount = 0
        showErrorCount = 0
        showLoadingCount = 0
        showRetryCount = 0
    }

    @Test
    fun testOnAttachView() {
        val stubView = object : StubCategoryView() {}
        val presenter = CategoryPresenter(mockFlowController, mockGetCategory, mockCategoryMapper, mockErrorMapper, mockCategory)
        presenter.onAttachView(stubView)
    }

    @Test
    fun testOnStart_success() {
        val stubView = object : StubCategoryView() {
            override fun showLoading() {
                showLoadingCount++
            }

            override fun showContent() {
                showContentCount++
            }

            override fun bindRoot(viewModel: CategoryViewModel) {
                assertThat(viewModel).isEqualTo(mockCategoryViewModel)
                bindRootCount++
            }

            override fun clearCategories() {
                clearCategoriesCount++
            }

            override fun bindCategory(viewModel: CategoryViewModel) {
                assertThat(viewModel).isEqualTo(mockCategoryViewModel)
                bindCategoryCount++
            }
        }

        whenever(mockGetCategory.execute(CATEGORY_NUMBER)).thenReturn(Observable.just(mockCategory))

        val presenter = CategoryPresenter(mockFlowController, mockGetCategory, mockCategoryMapper, mockErrorMapper, mockCategory)
        presenter.onAttachView(stubView)
        presenter.onStart()

        assertThat(bindRootCount).isEqualTo(1)
        assertThat(clearCategoriesCount).isEqualTo(1)
        assertThat(bindCategoryCount).isEqualTo(1)
        assertThat(showContentCount).isEqualTo(1)
        assertThat(showErrorCount).isEqualTo(0)
        assertThat(showLoadingCount).isEqualTo(1)
        assertThat(showRetryCount).isEqualTo(0)
    }

    @Test
    fun testOnStart_success_noPreviousCategory() {
        val stubView = object : StubCategoryView() {
            override fun showLoading() {
                showLoadingCount++
            }

            override fun showContent() {
                showContentCount++
            }

            override fun clearCategories() {
                clearCategoriesCount++
            }

            override fun bindCategory(viewModel: CategoryViewModel) {
                assertThat(viewModel).isEqualTo(mockCategoryViewModel)
                bindCategoryCount++
            }
        }

        whenever(mockGetCategory.execute(null)).thenReturn(Observable.just(mockCategory))

        val presenter = CategoryPresenter(mockFlowController, mockGetCategory, mockCategoryMapper, mockErrorMapper, null)
        presenter.onAttachView(stubView)
        presenter.onStart()

        assertThat(bindRootCount).isEqualTo(0)
        assertThat(clearCategoriesCount).isEqualTo(1)
        assertThat(bindCategoryCount).isEqualTo(1)
        assertThat(showContentCount).isEqualTo(1)
        assertThat(showErrorCount).isEqualTo(0)
        assertThat(showLoadingCount).isEqualTo(1)
        assertThat(showRetryCount).isEqualTo(0)
    }

    @Test
    fun testOnStart_error() {
        val stubView = object : StubCategoryView() {
            override fun showLoading() {
                showLoadingCount++
            }

            override fun bindRoot(viewModel: CategoryViewModel) {
                assertThat(viewModel).isEqualTo(mockCategoryViewModel)
                bindRootCount++
            }

            override fun clearCategories() {
                clearCategoriesCount++
            }

            override fun showError(viewModel: ErrorViewModel) {
                assertThat(viewModel).isEqualTo(mockErrorViewModel)
                showErrorCount++
            }

            override fun showRetry() {
                showRetryCount++
            }
        }

        whenever(mockGetCategory.execute(CATEGORY_NUMBER)).thenReturn(Observable.error(mockError))

        val presenter = CategoryPresenter(mockFlowController, mockGetCategory, mockCategoryMapper, mockErrorMapper, mockCategory)
        presenter.onAttachView(stubView)
        presenter.onStart()

        assertThat(bindRootCount).isEqualTo(1)
        assertThat(clearCategoriesCount).isEqualTo(1)
        assertThat(bindCategoryCount).isEqualTo(0)
        assertThat(showContentCount).isEqualTo(0)
        assertThat(showErrorCount).isEqualTo(1)
        assertThat(showLoadingCount).isEqualTo(1)
        assertThat(showRetryCount).isEqualTo(1)
    }

    @Test
    fun testOnStart_error_noPreviousCategory() {
        val stubView = object : StubCategoryView() {
            override fun showLoading() {
                showLoadingCount++
            }

            override fun clearCategories() {
                clearCategoriesCount++
            }

            override fun showError(viewModel: ErrorViewModel) {
                assertThat(viewModel).isEqualTo(mockErrorViewModel)
                showErrorCount++
            }

            override fun showRetry() {
                showRetryCount++
            }
        }

        whenever(mockGetCategory.execute(null)).thenReturn(Observable.error(mockError))

        val presenter = CategoryPresenter(mockFlowController, mockGetCategory, mockCategoryMapper, mockErrorMapper, null)
        presenter.onAttachView(stubView)
        presenter.onStart()

        assertThat(bindRootCount).isEqualTo(0)
        assertThat(clearCategoriesCount).isEqualTo(1)
        assertThat(bindCategoryCount).isEqualTo(0)
        assertThat(showContentCount).isEqualTo(0)
        assertThat(showErrorCount).isEqualTo(1)
        assertThat(showLoadingCount).isEqualTo(1)
        assertThat(showRetryCount).isEqualTo(1)
    }

    @Test
    fun testOnReloadClick_success() {
        val stubView = object : StubCategoryView() {
            override fun showLoading() {
                showLoadingCount++
            }

            override fun showContent() {
                showContentCount++
            }

            override fun clearCategories() {
                clearCategoriesCount++
            }

            override fun bindCategory(viewModel: CategoryViewModel) {
                assertThat(viewModel).isEqualTo(mockCategoryViewModel)
                bindCategoryCount++
            }
        }

        whenever(mockGetCategory.execute(CATEGORY_NUMBER)).thenReturn(Observable.just(mockCategory))

        val presenter = CategoryPresenter(mockFlowController, mockGetCategory, mockCategoryMapper, mockErrorMapper, mockCategory)
        presenter.onAttachView(stubView)
        presenter.onReloadClick()

        assertThat(bindRootCount).isEqualTo(0)
        assertThat(clearCategoriesCount).isEqualTo(1)
        assertThat(bindCategoryCount).isEqualTo(1)
        assertThat(showContentCount).isEqualTo(1)
        assertThat(showErrorCount).isEqualTo(0)
        assertThat(showLoadingCount).isEqualTo(1)
        assertThat(showRetryCount).isEqualTo(0)
    }

    @Test
    fun testOnReloadClick_success_noPreviousCategory() {
        val stubView = object : StubCategoryView() {
            override fun showLoading() {
                showLoadingCount++
            }

            override fun showContent() {
                showContentCount++
            }

            override fun clearCategories() {
                clearCategoriesCount++
            }

            override fun bindCategory(viewModel: CategoryViewModel) {
                assertThat(viewModel).isEqualTo(mockCategoryViewModel)
                bindCategoryCount++
            }
        }

        whenever(mockGetCategory.execute(null)).thenReturn(Observable.just(mockCategory))

        val presenter = CategoryPresenter(mockFlowController, mockGetCategory, mockCategoryMapper, mockErrorMapper, null)
        presenter.onAttachView(stubView)
        presenter.onReloadClick()

        assertThat(bindRootCount).isEqualTo(0)
        assertThat(clearCategoriesCount).isEqualTo(1)
        assertThat(bindCategoryCount).isEqualTo(1)
        assertThat(showContentCount).isEqualTo(1)
        assertThat(showErrorCount).isEqualTo(0)
        assertThat(showLoadingCount).isEqualTo(1)
        assertThat(showRetryCount).isEqualTo(0)
    }

    @Test
    fun testOnReloadClick_error() {
        val stubView = object : StubCategoryView() {
            override fun showLoading() {
                showLoadingCount++
            }

            override fun clearCategories() {
                clearCategoriesCount++
            }

            override fun showError(viewModel: ErrorViewModel) {
                assertThat(viewModel).isEqualTo(mockErrorViewModel)
                showErrorCount++
            }

            override fun showRetry() {
                showRetryCount++
            }
        }

        whenever(mockGetCategory.execute(CATEGORY_NUMBER)).thenReturn(Observable.error(mockError))

        val presenter = CategoryPresenter(mockFlowController, mockGetCategory, mockCategoryMapper, mockErrorMapper, mockCategory)
        presenter.onAttachView(stubView)
        presenter.onReloadClick()

        assertThat(bindRootCount).isEqualTo(0)
        assertThat(clearCategoriesCount).isEqualTo(1)
        assertThat(bindCategoryCount).isEqualTo(0)
        assertThat(showContentCount).isEqualTo(0)
        assertThat(showErrorCount).isEqualTo(1)
        assertThat(showLoadingCount).isEqualTo(1)
        assertThat(showRetryCount).isEqualTo(1)
    }

    @Test
    fun testOnReloadClick_error_noPreviousCategory() {
        val stubView = object : StubCategoryView() {
            override fun showLoading() {
                showLoadingCount++
            }

            override fun clearCategories() {
                clearCategoriesCount++
            }

            override fun showError(viewModel: ErrorViewModel) {
                assertThat(viewModel).isEqualTo(mockErrorViewModel)
                showErrorCount++
            }

            override fun showRetry() {
                showRetryCount++
            }
        }

        whenever(mockGetCategory.execute(null)).thenReturn(Observable.error(mockError))

        val presenter = CategoryPresenter(mockFlowController, mockGetCategory, mockCategoryMapper, mockErrorMapper, null)
        presenter.onAttachView(stubView)
        presenter.onReloadClick()

        assertThat(bindRootCount).isEqualTo(0)
        assertThat(clearCategoriesCount).isEqualTo(1)
        assertThat(bindCategoryCount).isEqualTo(0)
        assertThat(showContentCount).isEqualTo(0)
        assertThat(showErrorCount).isEqualTo(1)
        assertThat(showLoadingCount).isEqualTo(1)
        assertThat(showRetryCount).isEqualTo(1)
    }

    @Test(expected = IllegalStateException::class)
    fun testOnHelloWorldClick_unknownViewModel() {
        val presenter = CategoryPresenter(mockFlowController, mockGetCategory, mockCategoryMapper, mockErrorMapper, mockCategory)
        presenter.onCategoryClick(categoryViewModel())
    }

    @Test
    fun testOnHelloWorldClick_notLeaf() {
        mockCategory = mockCategory.copy(leaf = false)
        whenever(mockGetCategory.execute(CATEGORY_NUMBER)).thenReturn(Observable.just(mockCategory))

        val presenter = CategoryPresenter(mockFlowController, mockGetCategory, mockCategoryMapper, mockErrorMapper, mockCategory)

        presenter.onStart()
        presenter.onCategoryClick(categoryViewModel())

        verify(mockFlowController).launchCategory(mockCategory)
        verifyNoMoreInteractions(mockFlowController)
    }

    @Test
    fun testOnHelloWorldClick_leaf() {
        mockCategory = mockCategory.copy(leaf = true)
        whenever(mockGetCategory.execute(CATEGORY_NUMBER)).thenReturn(Observable.just(mockCategory))

        val presenter = CategoryPresenter(mockFlowController, mockGetCategory, mockCategoryMapper, mockErrorMapper, mockCategory)

        presenter.onStart()
        presenter.onCategoryClick(categoryViewModel())

        verify(mockFlowController).launchDetail(mockCategory)
        verifyNoMoreInteractions(mockFlowController)
    }

    @Test(expected = IllegalStateException::class)
    fun testOnHelloWorldClick_unknownViewModel_noPreviousCategory() {
        val presenter = CategoryPresenter(mockFlowController, mockGetCategory, mockCategoryMapper, mockErrorMapper, null)
        presenter.onCategoryClick(categoryViewModel())
    }

    @Test
    fun testOnHelloWorldClick_notLeaf_noPreviousCategory() {
        mockCategory = mockCategory.copy(leaf = false)
        whenever(mockGetCategory.execute(null)).thenReturn(Observable.just(mockCategory))

        val presenter = CategoryPresenter(mockFlowController, mockGetCategory, mockCategoryMapper, mockErrorMapper, null)

        presenter.onStart()
        presenter.onCategoryClick(categoryViewModel())

        verify(mockFlowController).launchCategory(mockCategory)
        verifyNoMoreInteractions(mockFlowController)
    }

    @Test
    fun testOnHelloWorldClick_leaf_noPreviousCategory() {
        mockCategory = mockCategory.copy(leaf = true)
        whenever(mockGetCategory.execute(null)).thenReturn(Observable.just(mockCategory))

        val presenter = CategoryPresenter(mockFlowController, mockGetCategory, mockCategoryMapper, mockErrorMapper, null)

        presenter.onStart()
        presenter.onCategoryClick(categoryViewModel())

        verify(mockFlowController).launchDetail(mockCategory)
        verifyNoMoreInteractions(mockFlowController)
    }

    @Test
    fun testOnSearchClick() {
        val presenter = CategoryPresenter(mockFlowController, mockGetCategory, mockCategoryMapper, mockErrorMapper, mockCategory)
        presenter.onSearchClick()
        verify(mockFlowController).launchSearch(mockCategory)
    }

    @Test
    fun testOnDetachView() {
        val presenter = CategoryPresenter(mockFlowController, mockGetCategory, mockCategoryMapper, mockErrorMapper, mockCategory)
        presenter.onDetachView()
    }

}
