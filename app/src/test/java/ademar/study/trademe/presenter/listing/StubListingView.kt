package ademar.study.trademe.presenter.listing

import ademar.study.trademe.model.ListingViewModel
import ademar.study.trademe.presenter.StubLoadDataView
import junit.framework.Assert.fail

open class StubListingView : StubLoadDataView(), ListingView {

    override fun bindListing(viewModel: ListingViewModel) = fail("Shouldn't call bindListing, viewModel $viewModel")

}
