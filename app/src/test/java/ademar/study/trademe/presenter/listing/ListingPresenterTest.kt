package ademar.study.trademe.presenter.listing

import ademar.study.trademe.core.interactor.GetListingDetail
import ademar.study.trademe.core.model.Listing
import ademar.study.trademe.mapper.ErrorMapper
import ademar.study.trademe.mapper.ListingMapper
import ademar.study.trademe.model.ErrorViewModel
import ademar.study.trademe.model.ListingViewModel
import ademar.study.trademe.test.BaseTest
import ademar.study.trademe.test.Fixture
import ademar.study.trademe.test.Fixture.errorViewModel
import ademar.study.trademe.test.Fixture.listing
import ademar.study.trademe.test.Fixture.listingDetail
import ademar.study.trademe.test.Fixture.listingViewModel
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Observable
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

class ListingPresenterTest : BaseTest() {

    @Mock lateinit var mockGetListingDetail: GetListingDetail
    @Mock lateinit var mockListingMapper: ListingMapper
    @Mock lateinit var mockErrorMapper: ErrorMapper

    private lateinit var mockListing: Listing
    private lateinit var mockError: Error
    private lateinit var mockErrorViewModel: ErrorViewModel

    private var bindListingCount = 0
    private var showContentCount = 0
    private var showErrorCount = 0
    private var showLoadingCount = 0
    private var showRetryCount = 0

    @Before
    override fun setUp() {
        super.setUp()

        mockListing = listing()
        mockError = Fixture.error()
        mockErrorViewModel = errorViewModel()

        whenever(mockListingMapper.transform(any())).thenReturn(listingViewModel())
        whenever(mockErrorMapper.transform(any())).thenReturn(mockErrorViewModel)

        bindListingCount = 0
        showContentCount = 0
        showErrorCount = 0
        showLoadingCount = 0
        showRetryCount = 0
    }

    @Test
    fun testOnAttachView() {
        val stubView = object : StubListingView() {}
        val presenter = ListingPresenter(mockContext, mockGetListingDetail, mockListingMapper, mockErrorMapper, mockListing)
        presenter.onAttachView(stubView)
    }

    @Test
    fun testOnStart_success() {
        val stubView = object : StubListingView() {
            override fun showLoading() {
                showLoadingCount++
            }

            override fun bindListing(viewModel: ListingViewModel) {
                assertThat(viewModel).isEqualTo(listingViewModel())
                bindListingCount++
            }

            override fun showContent() {
                showContentCount++
            }
        }

        whenever(mockGetListingDetail.execute(listing = listing())).thenReturn(Observable.just(listingDetail()))

        val presenter = ListingPresenter(mockContext, mockGetListingDetail, mockListingMapper, mockErrorMapper, mockListing)
        presenter.onAttachView(stubView)
        presenter.onStart()

        assertThat(bindListingCount).isEqualTo(1)
        assertThat(showContentCount).isEqualTo(1)
        assertThat(showErrorCount).isEqualTo(0)
        assertThat(showLoadingCount).isEqualTo(1)
        assertThat(showRetryCount).isEqualTo(0)
    }

    @Test
    fun testOnStart_noListing() {
        val stubView = object : StubListingView() {
            override fun showLoading() {
                showLoadingCount++
            }

            override fun showError(viewModel: ErrorViewModel) {
                assertThat(viewModel).isEqualTo(mockErrorViewModel)
                showErrorCount++
            }

            override fun showContent() {
                showContentCount++
            }

            override fun showRetry() {
                showRetryCount++
            }
        }

        val presenter = ListingPresenter(mockContext, mockGetListingDetail, mockListingMapper, mockErrorMapper, null)
        presenter.onAttachView(stubView)
        presenter.onStart()

        assertThat(bindListingCount).isEqualTo(0)
        assertThat(showContentCount).isEqualTo(0)
        assertThat(showErrorCount).isEqualTo(1)
        assertThat(showLoadingCount).isEqualTo(1)
        assertThat(showRetryCount).isEqualTo(1)
    }

    @Test
    fun testOnStart_error() {
        val stubView = object : StubListingView() {
            override fun showLoading() {
                showLoadingCount++
            }

            override fun showError(viewModel: ErrorViewModel) {
                assertThat(viewModel).isEqualTo(mockErrorViewModel)
                showErrorCount++
            }

            override fun showRetry() {
                showRetryCount++
            }
        }

        whenever(mockGetListingDetail.execute(listing = listing())).thenReturn(Observable.error(Fixture.error()))

        val presenter = ListingPresenter(mockContext, mockGetListingDetail, mockListingMapper, mockErrorMapper, mockListing)
        presenter.onAttachView(stubView)
        presenter.onStart()

        assertThat(bindListingCount).isEqualTo(0)
        assertThat(showContentCount).isEqualTo(0)
        assertThat(showErrorCount).isEqualTo(1)
        assertThat(showLoadingCount).isEqualTo(1)
        assertThat(showRetryCount).isEqualTo(1)
    }

    @Test
    fun testOnDetachView() {
        val presenter = ListingPresenter(mockContext, mockGetListingDetail, mockListingMapper, mockErrorMapper, mockListing)
        presenter.onDetachView()
    }

}
