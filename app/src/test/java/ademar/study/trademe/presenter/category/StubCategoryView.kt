package ademar.study.trademe.presenter.category

import ademar.study.trademe.model.CategoryViewModel
import ademar.study.trademe.presenter.StubLoadDataView
import org.assertj.core.api.Assertions.fail

open class StubCategoryView : StubLoadDataView(), CategoryView {

    override fun bindRoot(viewModel: CategoryViewModel) = fail("Shouldn't call bindRoot, viewModel $viewModel")

    override fun clearCategories() = fail("Shouldn't call clearCategories")

    override fun bindCategory(viewModel: CategoryViewModel) = fail("Shouldn't call bindCategory, viewModel $viewModel")

}
