package ademar.study.trademe.presenter.search

import ademar.study.trademe.model.SearchIntroViewModel
import ademar.study.trademe.model.SearchResultViewModel
import ademar.study.trademe.presenter.StubLoadDataView
import org.assertj.core.api.Assertions.fail

open class StubSearchView : StubLoadDataView(), SearchView {

    override fun bindIntro(viewModel: SearchIntroViewModel) = fail("Shouldn't call bindIntro, viewModel $viewModel")

    override fun clearResult() = fail("Shouldn't call clearResult")

    override fun showEmptyState() = fail("Shouldn't call showEmptyState")

    override fun bindResult(viewModel: SearchResultViewModel) = fail("Shouldn't call bindResult, viewModel $viewModel")


}
