package ademar.study.trademe.test

import ademar.study.trademe.core.model.*
import ademar.study.trademe.model.*

object Fixture {

    const val AMOUNT = "An amount"

    const val CATEGORY_AREA = 2
    const val CATEGORY_COUNT = 1
    const val CATEGORY_LEAF = true
    const val CATEGORY_NAME = "A Category"
    const val CATEGORY_NUMBER = "1234"
    const val CATEGORY_PATH = "/category"

    const val ERROR_EMPTY = "ERROR_EMPTY"
    const val ERROR_NO_CONNECTION = "ERROR_NO_CONNECTION"
    const val ERROR_UNAUTHORIZED = "ERROR_UNAUTHORIZED"
    const val ERROR_UNKNOWN = "ERROR_UNKNOWN"

    const val LISTING_DETAIL_CATEGORY_ID = "0001-0268-0310-"
    const val LISTING_DETAIL_ID = 6612200L
    const val LISTING_DETAIL_TITLE = "Mazda 626 1997"

    const val LISTING_ID = 6612200L

    const val LISTING_NAME = "A listing name"
    const val LISTING_PHOTO = "A listing photo"

    const val MESSAGE = "Hello World!"

    const val PHOTO_VALUE_FULL_SIZE = "https://images.tmsandbox.co.nz/photoserver/full/3529603.jpg"
    const val PHOTO_VALUE_GALLERY = "https://images.tmsandbox.co.nz/photoserver/gv/3529603.jpg"
    const val PHOTO_VALUE_LARGE = "https://images.tmsandbox.co.nz/photoserver/tq/3529603.jpg"
    const val PHOTO_VALUE_LIST = "https://images.tmsandbox.co.nz/photoserver/lv2/3529603.jpg"
    const val PHOTO_VALUE_MEDIUM = "https://images.tmsandbox.co.nz/photoserver/med/3529603.jpg"
    const val PHOTO_VALUE_THUMBNAIL = "https://images.tmsandbox.co.nz/photoserver/thumb/3529603.jpg"

    const val SEARCH_QUERY = "A query"
    const val SEARCH_INTRO_MESSAGE = "A search intro"
    const val SEARCH_INTRO_MESSAGE_CATEGORY = "A search intro with category"

    const val SEARCH_RESULT_MESSAGE = "A search result message"
    const val SEARCH_RESULT_MESSAGE_EMPTY = "A search result message when it is empty"
    const val SEARCH_RESULT_MESSAGE_MULTIPLE = "A search result message when it is multiple"

    const val TITLE = "A title"

    fun error() = Error(MESSAGE)

    fun category() = Category(
            CATEGORY_NAME,
            CATEGORY_NUMBER,
            CATEGORY_PATH,
            CATEGORY_LEAF,
            CATEGORY_AREA,
            CATEGORY_COUNT,
            listOf())

    fun photo() = Photo(photoValue())

    fun photoValue() = PhotoValue(PHOTO_VALUE_THUMBNAIL,
            PHOTO_VALUE_LIST,
            PHOTO_VALUE_MEDIUM,
            PHOTO_VALUE_GALLERY,
            PHOTO_VALUE_LARGE,
            PHOTO_VALUE_FULL_SIZE)

    fun listing() = Listing(LISTING_ID)

    fun listingDetail() = ListingDetail(
            LISTING_DETAIL_ID,
            LISTING_DETAIL_TITLE,
            LISTING_DETAIL_CATEGORY_ID,
            listOf(photo()))

    fun errorViewModel() = ErrorViewModel(MESSAGE)

    fun categoryViewModel() = CategoryViewModel(TITLE, AMOUNT)

    fun searchIntroViewModel() = SearchIntroViewModel(SEARCH_INTRO_MESSAGE)

    fun listingViewModel() = ListingViewModel(LISTING_NAME, listOf(LISTING_PHOTO))

    fun searchResultViewModel() = SearchResultViewModel(SEARCH_RESULT_MESSAGE, listOf(listing()))

}
