package ademar.study.trademe.test

import ademar.study.trademe.R
import ademar.study.trademe.test.Fixture.ERROR_EMPTY
import ademar.study.trademe.test.Fixture.ERROR_NO_CONNECTION
import ademar.study.trademe.test.Fixture.ERROR_UNAUTHORIZED
import ademar.study.trademe.test.Fixture.ERROR_UNKNOWN
import android.content.Context
import android.content.res.Resources
import android.support.annotation.CallSuper
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.assertj.core.api.Assertions.fail
import org.junit.After
import org.junit.Before
import org.mockito.Mock
import org.mockito.MockitoAnnotations

abstract class BaseTest {

    @Mock lateinit var mockContext: Context
    @Mock lateinit var mockResources: Resources

    private var rxError: Throwable? = null

    @Before
    @CallSuper
    open fun setUp() {
        MockitoAnnotations.initMocks(this)
        rxError = null

        RxAndroidPlugins.setInitMainThreadSchedulerHandler {
            Schedulers.trampoline()
        }
        RxJavaPlugins.setIoSchedulerHandler {
            Schedulers.trampoline()
        }
        RxJavaPlugins.setErrorHandler { error ->
            println("Received error $error stacktrace:")
            error.printStackTrace()
            rxError = error
        }

        whenever(mockContext.resources).thenReturn(mockResources)
        whenever(mockContext.getString(R.string.error_message_empty_listings)).thenReturn(ERROR_EMPTY)
        whenever(mockContext.getString(R.string.error_message_unknown)).thenReturn(ERROR_UNKNOWN)
        whenever(mockContext.getString(R.string.error_message_unauthorized)).thenReturn(ERROR_UNAUTHORIZED)
        whenever(mockContext.getString(R.string.error_message_no_connection)).thenReturn(ERROR_NO_CONNECTION)
    }

    @After
    @CallSuper
    open fun tearDown() {
        if (rxError != null) {
            fail("Error on rx: $rxError")
        }
        RxAndroidPlugins.reset()
        RxJavaPlugins.reset()
    }

}
