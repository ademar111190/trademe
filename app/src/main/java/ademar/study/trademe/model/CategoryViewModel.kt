package ademar.study.trademe.model

data class CategoryViewModel(

        val title: String,
        val amount: String

)
