package ademar.study.trademe.model

data class ListingViewModel(

        val name: String,
        val photos: List<String>

)
