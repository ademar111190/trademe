package ademar.study.trademe.model

import ademar.study.trademe.core.model.Listing

data class SearchResultViewModel(

        val message: String,
        val listings: List<Listing>

)
