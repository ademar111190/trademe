package ademar.study.trademe

import ademar.study.trademe.core.injection.CoreComponent
import ademar.study.trademe.core.injection.CoreModule
import ademar.study.trademe.core.injection.DaggerCoreComponent
import android.app.Application

class App : Application() {

    val coreComponent: CoreComponent by lazy {
        DaggerCoreComponent.builder()
                .coreModule(CoreModule(this, BuildConfig.API_URL))
                .build()
    }

}
