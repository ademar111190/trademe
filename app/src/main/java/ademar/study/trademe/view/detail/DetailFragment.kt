package ademar.study.trademe.view.detail

import ademar.study.trademe.R
import ademar.study.trademe.core.model.Listing
import ademar.study.trademe.injection.LifeCycleModule
import ademar.study.trademe.model.ErrorViewModel
import ademar.study.trademe.navigation.ARG_CATEGORY
import ademar.study.trademe.presenter.detail.DetailPresenter
import ademar.study.trademe.presenter.detail.DetailView
import ademar.study.trademe.view.base.BaseFragment
import ademar.study.trademe.view.listing.ListingAdapter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import kotlinx.android.synthetic.main.detail_fragment.*
import javax.inject.Inject

class DetailFragment : BaseFragment(), DetailView {

    @Inject lateinit var presenter: DetailPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.detail_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        component.inject(this)
        presenter.onAttachView(this)

        if (context?.resources?.getBoolean(R.bool.large_screen) != true) {
            toolbar.setNavigationIcon(R.drawable.ic_back)
            toolbar.setNavigationOnClickListener { getBaseActivity().back() }
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.onStart()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.onDetachView()
    }

    override fun makeLifeCycleModule() = LifeCycleModule(getBaseActivity(), category = arguments?.getParcelable(ARG_CATEGORY))

    override fun showLoading() {
        pager.visibility = GONE
        load.visibility = VISIBLE
        error.visibility = GONE
    }

    override fun showRetry() {
        pager.visibility = GONE
        load.visibility = GONE
        error.visibility = VISIBLE
    }

    override fun showContent() {
        pager.visibility = VISIBLE
        load.visibility = GONE
        error.visibility = GONE
    }

    override fun showError(viewModel: ErrorViewModel) {
        error.text = viewModel.message
    }

    override fun bindListings(listings: List<Listing>) {
        pager.adapter = ListingAdapter(fragmentManager, listings)
    }

}
