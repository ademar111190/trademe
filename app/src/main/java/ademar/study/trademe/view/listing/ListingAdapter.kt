package ademar.study.trademe.view.listing

import ademar.study.trademe.core.model.Listing
import ademar.study.trademe.navigation.ARG_LISTING
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import androidx.core.os.bundleOf

class ListingAdapter(

        fragmentManager: FragmentManager?,
        private val listings: List<Listing>

) : FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int) = ListingFragment().apply {
        arguments = bundleOf(ARG_LISTING to listings[position])
    }

    override fun getCount() = listings.size

}
