package ademar.study.trademe.view.search

import ademar.study.trademe.R
import ademar.study.trademe.injection.LifeCycleModule
import ademar.study.trademe.model.ErrorViewModel
import ademar.study.trademe.model.SearchIntroViewModel
import ademar.study.trademe.model.SearchResultViewModel
import ademar.study.trademe.navigation.ARG_CATEGORY
import ademar.study.trademe.presenter.search.SearchPresenter
import ademar.study.trademe.presenter.search.SearchView
import ademar.study.trademe.view.base.BaseFragment
import ademar.study.trademe.view.listing.ListingAdapter
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.search_fragment.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SearchFragment : BaseFragment(), SearchView {

    @Inject lateinit var presenter: SearchPresenter

    private val subscriptions = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.search_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        component.inject(this)
        presenter.onAttachView(this)

        toolbar.setNavigationOnClickListener { getBaseActivity().back() }
        reload.setOnClickListener { presenter.search(search.text.toString()) }

        subscriptions.add(RxTextView.textChanges(search)
                .debounce(500L, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe({ search ->
                    presenter.search(search.toString())
                }, { e ->
                    e.printStackTrace()
                }))

        search.setOnEditorActionListener { _, _, _ ->
            val inputMethodManager = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager?
            inputMethodManager?.hideSoftInputFromWindow(search.windowToken, 0)
            true
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.onStart()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.onDetachView()
        subscriptions.clear()
    }

    override fun makeLifeCycleModule() = LifeCycleModule(getBaseActivity(), category = arguments?.getParcelable(ARG_CATEGORY))

    override fun showLoading() {
        pager.visibility = GONE
        load.visibility = VISIBLE
        message.visibility = GONE
        reload.visibility = GONE
    }

    override fun showRetry() {
        pager.visibility = GONE
        load.visibility = GONE
        message.visibility = VISIBLE
        reload.visibility = VISIBLE
    }

    override fun showContent() {
        pager.visibility = VISIBLE
        load.visibility = GONE
        message.visibility = GONE
        reload.visibility = GONE
    }

    override fun showEmptyState() {
        pager.visibility = GONE
        load.visibility = GONE
        message.visibility = VISIBLE
        reload.visibility = GONE
    }

    override fun showError(viewModel: ErrorViewModel) {
        message.text = viewModel.message
    }

    override fun bindIntro(viewModel: SearchIntroViewModel) {
        message.text = viewModel.message
    }

    override fun bindResult(viewModel: SearchResultViewModel) {
        pager.adapter = ListingAdapter(fragmentManager, viewModel.listings)
    }

    override fun clearResult() {
        pager.adapter = ListingAdapter(fragmentManager, listOf())
    }

}
