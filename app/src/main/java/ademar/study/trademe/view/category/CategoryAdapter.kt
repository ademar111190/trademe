package ademar.study.trademe.view.category

import ademar.study.trademe.R
import ademar.study.trademe.ext.inflate
import ademar.study.trademe.model.CategoryViewModel
import ademar.study.trademe.view.base.BaseAdapter
import android.view.ViewGroup
import javax.inject.Inject

class CategoryAdapter @Inject constructor() : BaseAdapter<CategoryViewModel, CategoryViewHolder>() {

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CategoryViewHolder(parent.inflate(R.layout.category_item))

    override fun getItemId(position: Int) = data[position].hashCode().toLong()

    override fun addItem(item: CategoryViewModel) {
        data.add(item)
        data.sortBy { it.title }
        notifyItemInserted(data.indexOf(item))
    }

}
