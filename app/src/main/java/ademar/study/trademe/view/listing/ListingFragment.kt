package ademar.study.trademe.view.listing

import ademar.study.trademe.R
import ademar.study.trademe.injection.GlideApp
import ademar.study.trademe.injection.LifeCycleModule
import ademar.study.trademe.model.ListingViewModel
import ademar.study.trademe.navigation.ARG_LISTING
import ademar.study.trademe.presenter.listing.ListingPresenter
import ademar.study.trademe.presenter.listing.ListingView
import ademar.study.trademe.view.base.BaseFragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.synthetic.main.listing_fragment.*
import javax.inject.Inject

class ListingFragment : BaseFragment(), ListingView {

    @Inject lateinit var presenter: ListingPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.listing_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        component.inject(this)
        presenter.onAttachView(this)
    }

    override fun onResume() {
        super.onResume()
        presenter.onStart()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.onDetachView()
    }

    override fun makeLifeCycleModule() = LifeCycleModule(getBaseActivity(), listing = arguments?.getParcelable(ARG_LISTING))

    override fun showLoading() {
        title.visibility = View.GONE
        load.visibility = View.VISIBLE
    }

    override fun showRetry() {
        title.visibility = View.GONE
        load.visibility = View.GONE
    }

    override fun showContent() {
        title.visibility = View.VISIBLE
        load.visibility = View.GONE
    }

    override fun bindListing(viewModel: ListingViewModel) {
        title.text = viewModel.name
        viewModel.photos.firstOrNull()?.let {
            GlideApp.with(this)
                    .load(it)
                    .centerInside()
                    .placeholder(R.drawable.ic_launcher_background)
                    .error(R.drawable.ic_launcher_background)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(image)
        }
    }

}
