package ademar.study.trademe.view.category

import ademar.study.trademe.R
import ademar.study.trademe.navigation.FlowController
import ademar.study.trademe.view.base.BaseActivity
import android.os.Bundle
import javax.inject.Inject

class CategoryActivity : BaseActivity() {

    @Inject lateinit var flowController: FlowController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.category_activity)
        prepareTaskDescription()
        component.inject(this)
        flowController.launchCategory(this)
    }

}
