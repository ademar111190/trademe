package ademar.study.trademe.view.category

import ademar.study.trademe.model.CategoryViewModel
import ademar.study.trademe.view.base.BaseViewHolder
import android.view.View
import androidx.core.text.bold
import androidx.core.text.buildSpannedString
import androidx.core.text.italic
import kotlinx.android.synthetic.main.category_item.view.*

class CategoryViewHolder(itemView: View) : BaseViewHolder<CategoryViewModel>(itemView) {

    override fun bind(viewModel: CategoryViewModel, listener: () -> Unit) {
        itemView.text.text = buildSpannedString {
            bold { append(viewModel.title) }
            append(" ")
            italic { append(viewModel.amount) }
        }
        itemView.setOnClickListener { listener() }
    }

}
