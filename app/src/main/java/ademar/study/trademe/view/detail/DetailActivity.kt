package ademar.study.trademe.view.detail

import ademar.study.trademe.R
import ademar.study.trademe.navigation.FlowController
import ademar.study.trademe.view.base.BaseActivity
import android.os.Bundle
import javax.inject.Inject

class DetailActivity : BaseActivity() {

    @Inject lateinit var flowController: FlowController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.detail_activity)
        prepareTaskDescription()
        component.inject(this)
        flowController.launchDetail(this)
    }

}
