package ademar.study.trademe.view.base

import ademar.study.trademe.App
import ademar.study.trademe.R
import ademar.study.trademe.injection.DaggerLifeCycleComponent
import ademar.study.trademe.injection.LifeCycleComponent
import ademar.study.trademe.injection.LifeCycleModule
import ademar.study.trademe.model.ErrorViewModel
import ademar.study.trademe.presenter.LoadDataView
import android.app.ActivityManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.support.v4.app.NavUtils
import android.support.v4.app.TaskStackBuilder
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity(), LoadDataView {

    protected val component: LifeCycleComponent by lazy {
        DaggerLifeCycleComponent.builder()
                .coreComponent(getApp().coreComponent)
                .lifeCycleModule(makeLifeCycleModule())
                .build()
    }

    protected open fun makeLifeCycleModule() = LifeCycleModule(this)

    protected fun prepareTaskDescription(
            label: String = getString(R.string.app_name),
            icon: Int = R.drawable.ic_task,
            colorPrimary: Int = ContextCompat.getColor(this, R.color.primary)
    ) {
        val drawable = ContextCompat.getDrawable(this, icon)
        if (drawable != null) {
            val bitmap = Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(bitmap)
            drawable.setBounds(0, 0, canvas.width, canvas.height)
            drawable.draw(canvas)
            setTaskDescription(ActivityManager.TaskDescription(label, bitmap, colorPrimary))
        }
    }

    private fun getApp() = applicationContext as App

    override fun getBaseActivity() = this

    override fun showLoading() {
    }

    override fun showRetry() {
    }

    override fun showContent() {
    }

    override fun showError(viewModel: ErrorViewModel) = AlertDialog
            .Builder(this, R.style.AppAlertDialog)
            .setMessage(viewModel.message)
            .setPositiveButton(R.string.app_ok, null)
            .create()
            .show()

    override fun onBackPressed() {
        back()
    }

    fun back() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
        } else {
            val upIntent = NavUtils.getParentActivityIntent(this)
            if (upIntent == null) {
                finish()
                return
            }
            if (NavUtils.shouldUpRecreateTask(this, upIntent) || isTaskRoot) {
                TaskStackBuilder.create(this)
                        .addNextIntentWithParentStack(upIntent)
                        .startActivities()
            } else {
                NavUtils.navigateUpTo(this, upIntent)
            }
        }
    }

}
