package ademar.study.trademe.view.category

import ademar.study.trademe.R
import ademar.study.trademe.injection.LifeCycleModule
import ademar.study.trademe.model.CategoryViewModel
import ademar.study.trademe.navigation.ARG_CATEGORY
import ademar.study.trademe.presenter.category.CategoryPresenter
import ademar.study.trademe.presenter.category.CategoryView
import ademar.study.trademe.view.base.BaseFragment
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.category_fragment.*
import javax.inject.Inject

class CategoryFragment : BaseFragment(), CategoryView {

    @Inject lateinit var presenter: CategoryPresenter
    @Inject lateinit var adapter: CategoryAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater
            .inflate(R.layout.category_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        component.inject(this)

        reload.setOnClickListener { presenter.onReloadClick() }
        listRefresh.setOnRefreshListener { presenter.onReloadClick() }
        adapter.listener = presenter::onCategoryClick

        list.layoutManager = LinearLayoutManager(context)
        list.adapter = adapter

        toolbar.inflateMenu(R.menu.search)
        toolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.search -> {
                    presenter.onSearchClick()
                    true
                }
                else -> false
            }
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.onAttachView(this)
        presenter.onStart()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.onDetachView()
    }

    override fun makeLifeCycleModule() = LifeCycleModule(getBaseActivity(), category = arguments?.getParcelable(ARG_CATEGORY))

    override fun showLoading() {
        list.visibility = View.GONE
        load.visibility = View.VISIBLE
        reload.visibility = View.GONE
        listRefresh.isRefreshing = false
    }

    override fun showRetry() {
        list.visibility = View.GONE
        load.visibility = View.GONE
        reload.visibility = View.VISIBLE
        listRefresh.isRefreshing = false
    }

    override fun showContent() {
        list.visibility = View.VISIBLE
        load.visibility = View.GONE
        reload.visibility = View.GONE
        listRefresh.isRefreshing = false
    }

    override fun bindRoot(viewModel: CategoryViewModel) {
        toolbar.title = viewModel.title
        toolbar.setNavigationIcon(R.drawable.ic_back)
        toolbar.setNavigationOnClickListener { getBaseActivity().back() }
    }

    override fun clearCategories() {
        adapter.clear()
    }

    override fun bindCategory(viewModel: CategoryViewModel) {
        adapter.addItem(viewModel)
    }

}
