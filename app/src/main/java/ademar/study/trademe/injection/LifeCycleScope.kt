package ademar.study.trademe.injection

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class LifeCycleScope
