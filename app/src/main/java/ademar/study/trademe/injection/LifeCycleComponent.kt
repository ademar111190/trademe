package ademar.study.trademe.injection

import ademar.study.trademe.core.injection.CoreComponent
import ademar.study.trademe.view.category.CategoryActivity
import ademar.study.trademe.view.category.CategoryFragment
import ademar.study.trademe.view.common.StartActivity
import ademar.study.trademe.view.detail.DetailActivity
import ademar.study.trademe.view.detail.DetailFragment
import ademar.study.trademe.view.listing.ListingFragment
import ademar.study.trademe.view.search.SearchActivity
import ademar.study.trademe.view.search.SearchFragment
import dagger.Component

@LifeCycleScope
@Component(modules = [LifeCycleModule::class], dependencies = [CoreComponent::class])
interface LifeCycleComponent {

    fun inject(o: CategoryActivity)
    fun inject(o: DetailActivity)
    fun inject(o: SearchActivity)
    fun inject(o: StartActivity)

    fun inject(o: CategoryFragment)
    fun inject(o: DetailFragment)
    fun inject(o: ListingFragment)
    fun inject(o: SearchFragment)

}
