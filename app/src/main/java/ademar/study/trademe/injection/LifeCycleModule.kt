package ademar.study.trademe.injection

import ademar.study.trademe.core.model.Category
import ademar.study.trademe.core.model.Listing
import ademar.study.trademe.view.base.BaseActivity
import dagger.Module
import dagger.Provides

@Module
class LifeCycleModule(

        private val baseActivity: BaseActivity,
        private val category: Category? = null,
        private val listing: Listing? = null

) {

    @Provides
    @LifeCycleScope
    fun provideBaseActivity() = baseActivity

    @Provides
    @LifeCycleScope
    fun provideCategory() = category

    @Provides
    @LifeCycleScope
    fun provideListing() = listing

}
