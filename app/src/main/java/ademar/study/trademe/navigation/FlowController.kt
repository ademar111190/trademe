package ademar.study.trademe.navigation

import ademar.study.trademe.R
import ademar.study.trademe.core.model.Category
import ademar.study.trademe.view.base.BaseActivity
import ademar.study.trademe.view.category.CategoryActivity
import ademar.study.trademe.view.category.CategoryFragment
import ademar.study.trademe.view.common.WelcomeFragment
import ademar.study.trademe.view.detail.DetailActivity
import ademar.study.trademe.view.detail.DetailFragment
import ademar.study.trademe.view.search.SearchActivity
import ademar.study.trademe.view.search.SearchFragment
import android.os.Bundle
import androidx.core.os.bundleOf
import javax.inject.Inject

class FlowController @Inject constructor(

        private val context: BaseActivity,
        private val intentFactory: IntentFactory

) {

    fun launchHome() = context.startActivity(intentFactory.makeIntent().apply {
        setClassName(context, CategoryActivity::class.java.name)
    })

    fun launchCategory(categoryActivity: CategoryActivity) {
        launchCategoryFragment(categoryActivity.intent.extras ?: bundleOf())
        if (context.resources.getBoolean(R.bool.large_screen)) {
            context.supportFragmentManager.beginTransaction()
                    .replace(R.id.detail_fragment, WelcomeFragment())
                    .commit()
        }
    }

    fun launchCategory(category: Category) {
        val bundle = bundleOf(ARG_CATEGORY to category)
        if (context.resources.getBoolean(R.bool.large_screen)) {
            launchCategoryFragment(bundle, category.name)
        } else {
            context.startActivity(intentFactory.makeIntent().apply {
                setClassName(context, CategoryActivity::class.java.name)
                putExtras(bundle)
            })
        }
    }

    fun launchDetail(detailActivity: DetailActivity) {
        launchDetailFragment(detailActivity.intent.extras)
    }

    fun launchDetail(category: Category) {
        val bundle = bundleOf(ARG_CATEGORY to category)
        if (context.resources.getBoolean(R.bool.large_screen)) {
            launchDetailFragment(bundle)
        } else {
            context.startActivity(intentFactory.makeIntent().apply {
                setClassName(context, DetailActivity::class.java.name)
                putExtras(bundle)
            })
        }
    }

    fun launchSearch(searchActivity: SearchActivity) {
        launchSearchFragment(searchActivity.intent.extras)
    }

    fun launchSearch(category: Category?) {
        val bundle = bundleOf(ARG_CATEGORY to category)
        context.startActivity(intentFactory.makeIntent().apply {
            setClassName(context, SearchActivity::class.java.name)
            putExtras(bundle)
        })
    }

    private fun launchCategoryFragment(bundle: Bundle, stack: String? = null) {
        context.supportFragmentManager.beginTransaction()
                .replace(R.id.category_fragment, CategoryFragment().apply {
                    arguments = bundle
                }).apply {
                    if (stack != null) {
                        addToBackStack(stack)
                    }
                }
                .commit()
    }

    private fun launchDetailFragment(bundle: Bundle) {
        context.supportFragmentManager.beginTransaction()
                .replace(R.id.detail_fragment, DetailFragment().apply {
                    arguments = bundle
                })
                .commit()
    }

    private fun launchSearchFragment(bundle: Bundle) {
        context.supportFragmentManager.beginTransaction()
                .replace(R.id.search_fragment, SearchFragment().apply {
                    arguments = bundle
                })
                .commit()
    }

}

const val ARG_CATEGORY = "ARG_CATEGORY"
const val ARG_LISTING = "ARG_LISTING"
