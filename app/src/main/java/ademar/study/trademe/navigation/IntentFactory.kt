package ademar.study.trademe.navigation

import android.content.Intent
import javax.inject.Inject

class IntentFactory @Inject constructor() {

    fun makeIntent() = Intent()

}
