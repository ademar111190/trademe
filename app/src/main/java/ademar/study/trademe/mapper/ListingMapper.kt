package ademar.study.trademe.mapper

import ademar.study.trademe.core.model.ListingDetail
import ademar.study.trademe.model.ListingViewModel
import javax.inject.Inject

class ListingMapper @Inject constructor() {

    fun transform(listing: ListingDetail) = ListingViewModel(
            listing.title,
            listing.photos
                    .map { it.value }
                    .mapNotNull {
                        listOf(
                                it.fullSize,
                                it.large,
                                it.gallery,
                                it.medium,
                                it.list,
                                it.thumbnail
                        ).firstOrNull { it.isNotBlank() }
                    })

}
