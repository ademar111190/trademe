package ademar.study.trademe.mapper

import ademar.study.trademe.core.model.Category
import ademar.study.trademe.model.CategoryViewModel
import javax.inject.Inject

class CategoryMapper @Inject constructor() {

    fun transform(category: Category) = CategoryViewModel(category.name, "(${category.count})")

}
