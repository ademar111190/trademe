package ademar.study.trademe.mapper

import ademar.study.trademe.R
import ademar.study.trademe.core.model.Category
import ademar.study.trademe.model.SearchIntroViewModel
import android.content.Context
import javax.inject.Inject

class SearchIntroMapper @Inject constructor(

        private val context: Context

) {

    fun transform(category: Category? = null) = SearchIntroViewModel(if (category == null) {
        context.getString(R.string.search_intro)
    } else {
        context.getString(R.string.search_intro_catalog, category.name)
    })

}
