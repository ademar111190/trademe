package ademar.study.trademe.mapper

import ademar.study.trademe.R
import ademar.study.trademe.core.model.Listing
import ademar.study.trademe.model.SearchResultViewModel
import android.content.Context
import javax.inject.Inject

class SearchResultMapper @Inject constructor(

        private val context: Context

) {

    fun transform(listings: List<Listing>) = SearchResultViewModel(
            context.resources.getQuantityString(R.plurals.search_result, listings.size), listings)

}
