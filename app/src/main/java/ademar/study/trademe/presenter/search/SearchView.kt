package ademar.study.trademe.presenter.search

import ademar.study.trademe.model.SearchIntroViewModel
import ademar.study.trademe.model.SearchResultViewModel
import ademar.study.trademe.presenter.LoadDataView

interface SearchView : LoadDataView {

    fun bindIntro(viewModel: SearchIntroViewModel)

    fun clearResult()

    fun showEmptyState()

    fun bindResult(viewModel: SearchResultViewModel)

}
