package ademar.study.trademe.presenter.listing

import ademar.study.trademe.R
import ademar.study.trademe.core.interactor.GetListingDetail
import ademar.study.trademe.core.model.Listing
import ademar.study.trademe.core.model.ListingDetail
import ademar.study.trademe.injection.LifeCycleScope
import ademar.study.trademe.mapper.ErrorMapper
import ademar.study.trademe.mapper.ListingMapper
import ademar.study.trademe.presenter.BasePresenter
import android.content.Context
import android.util.Log
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@LifeCycleScope
class ListingPresenter @Inject constructor(

        private val context: Context,
        private val getListingDetail: GetListingDetail,
        private val listingMapper: ListingMapper,
        private val errorMapper: ErrorMapper,
        private val listing: Listing?

) : BasePresenter<ListingView>() {

    fun onStart() {
        view?.showLoading()

        subscriptions.add((
                if (listing != null) {
                    getListingDetail.execute(listing)
                } else {
                    Log.e("ListingPresenter", "ListingPresenter created without a Listing.")
                    Observable.error<ListingDetail>(Error(context.getString(R.string.error_message_unknown)))
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view?.bindListing(listingMapper.transform(it))
                    view?.showContent()
                }, { e ->
                    view?.showError(errorMapper.transform(e))
                    view?.showRetry()
                }))
    }

}
