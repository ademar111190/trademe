package ademar.study.trademe.presenter.detail

import ademar.study.trademe.R
import ademar.study.trademe.core.interactor.SearchListing
import ademar.study.trademe.core.model.Category
import ademar.study.trademe.core.model.Listing
import ademar.study.trademe.injection.LifeCycleScope
import ademar.study.trademe.mapper.ErrorMapper
import ademar.study.trademe.presenter.BasePresenter
import android.content.Context
import android.util.Log
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@LifeCycleScope
class DetailPresenter @Inject constructor(

        private val context: Context,
        private val searchListing: SearchListing,
        private val errorMapper: ErrorMapper,
        private val category: Category?

) : BasePresenter<DetailView>() {

    fun onStart() {
        view?.showLoading()

        subscriptions.add((
                if (category != null) {
                    searchListing.execute(category = category)
                } else {
                    Log.e("DetailPresenter", "DetailPresenter created without a Category.")
                    Observable.error<Listing>(Error(context.getString(R.string.error_message_unknown)))
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .toList()
                .subscribe({
                    if (it.isEmpty()) {
                        view?.showError(errorMapper.transform(Error(context.getString(R.string.error_message_empty_listings))))
                        view?.showRetry()
                    } else {
                        view?.bindListings(it)
                        view?.showContent()
                    }
                }, { e ->
                    view?.showError(errorMapper.transform(e))
                    view?.showRetry()
                }))
    }

}
