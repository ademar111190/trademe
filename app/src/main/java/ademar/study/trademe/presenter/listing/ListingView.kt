package ademar.study.trademe.presenter.listing

import ademar.study.trademe.model.ListingViewModel
import ademar.study.trademe.presenter.LoadDataView

interface ListingView : LoadDataView {

    fun bindListing(viewModel: ListingViewModel)

}
