package ademar.study.trademe.presenter.detail

import ademar.study.trademe.core.model.Listing
import ademar.study.trademe.presenter.LoadDataView

interface DetailView : LoadDataView {

    fun bindListings(listings: List<Listing>)

}
