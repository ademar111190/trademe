package ademar.study.trademe.presenter

import ademar.study.trademe.model.ErrorViewModel
import ademar.study.trademe.view.base.BaseActivity

interface LoadDataView {

    fun getBaseActivity(): BaseActivity?

    fun showLoading()

    fun showRetry()

    fun showContent()

    fun showError(viewModel: ErrorViewModel)

}
