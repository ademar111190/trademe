package ademar.study.trademe.presenter.category

import ademar.study.trademe.core.interactor.GetCategory
import ademar.study.trademe.core.model.Category
import ademar.study.trademe.injection.LifeCycleScope
import ademar.study.trademe.mapper.CategoryMapper
import ademar.study.trademe.mapper.ErrorMapper
import ademar.study.trademe.model.CategoryViewModel
import ademar.study.trademe.navigation.FlowController
import ademar.study.trademe.presenter.BasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@LifeCycleScope
class CategoryPresenter @Inject constructor(

        private val flowController: FlowController,
        private val getCategory: GetCategory,
        private val categoryMapper: CategoryMapper,
        private val errorMapper: ErrorMapper,
        private val category: Category?

) : BasePresenter<CategoryView>() {

    private val map = linkedMapOf<CategoryViewModel, Category>()

    fun onStart() {
        if (category != null) {
            view?.bindRoot(categoryMapper.transform(category))
        }
        loadData()
    }

    fun onReloadClick() = loadData()

    fun onCategoryClick(viewModel: CategoryViewModel) {
        val category = map[viewModel] ?: throw IllegalStateException("The map $map doesn't know the viewModel item $viewModel")
        if (category.leaf) {
            flowController.launchDetail(category)
        } else {
            flowController.launchCategory(category)
        }
    }

    fun onSearchClick() {
        flowController.launchSearch(category)
    }

    private fun loadData() {
        view?.showLoading()
        view?.clearCategories()
        subscriptions.add(getCategory.execute(category?.number)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    val viewModel = categoryMapper.transform(it)
                    map[viewModel] = it
                    view?.bindCategory(viewModel)
                    view?.showContent()
                }, { e ->
                    view?.showError(errorMapper.transform(e))
                    view?.showRetry()
                }))
    }

}
