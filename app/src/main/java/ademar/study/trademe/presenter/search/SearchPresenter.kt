package ademar.study.trademe.presenter.search

import ademar.study.trademe.core.interactor.SearchListing
import ademar.study.trademe.core.model.Category
import ademar.study.trademe.injection.LifeCycleScope
import ademar.study.trademe.mapper.ErrorMapper
import ademar.study.trademe.mapper.SearchIntroMapper
import ademar.study.trademe.mapper.SearchResultMapper
import ademar.study.trademe.presenter.BasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@LifeCycleScope
class SearchPresenter @Inject constructor(

        private val searchListing: SearchListing,
        private val searchIntroMapper: SearchIntroMapper,
        private val searchResultMapper: SearchResultMapper,
        private val errorMapper: ErrorMapper,
        private val category: Category?

) : BasePresenter<SearchView>() {

    fun onStart() = search("")

    fun search(query: String) {
        view?.showLoading()
        view?.clearResult()

        subscriptions.clear()

        if (query.isBlank()) {
            view?.bindIntro(searchIntroMapper.transform(category))
            view?.showEmptyState()
        } else {
            subscriptions.add(searchListing.execute(query, category)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .toList()
                    .subscribe({
                        view?.bindResult(searchResultMapper.transform(it))
                        view?.showContent()
                    }, { e ->
                        view?.showError(errorMapper.transform(e))
                        view?.showRetry()
                    }))
        }
    }

}
