package ademar.study.trademe.presenter.category

import ademar.study.trademe.model.CategoryViewModel
import ademar.study.trademe.presenter.LoadDataView

interface CategoryView : LoadDataView {

    fun bindRoot(viewModel: CategoryViewModel)

    fun clearCategories()

    fun bindCategory(viewModel: CategoryViewModel)

}
