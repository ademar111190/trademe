package ademar.study.trademe.core.model

import ademar.study.trademe.core.injection.ApplicationJsonAdapterFactory
import ademar.study.trademe.core.test.BaseTest
import ademar.study.trademe.core.test.Fixture.listing
import ademar.study.trademe.core.test.Fixture.search
import ademar.study.trademe.core.test.containsJsonList
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test

class SearchTest : BaseTest() {

    private lateinit var adapter: JsonAdapter<Search>

    @Before
    override fun setUp() {
        super.setUp()
        adapter = Moshi.Builder()
                .add(ApplicationJsonAdapterFactory.INSTANCE)
                .build()
                .adapter(Search::class.java)
    }

    @Test
    fun testParse() {
        val search = adapter.fromJson(readJson("search")) ?: throw IllegalStateException("Failed to parse")
        assertThat(search.result).isEqualTo(listOf(listing()))
    }

    @Test
    fun testSerialize() {
        val json = adapter.toJson(search())
        assertThat(json).containsJsonList("List")
    }

}
