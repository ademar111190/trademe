package ademar.study.trademe.core.model

import ademar.study.trademe.core.injection.ApplicationJsonAdapterFactory
import ademar.study.trademe.core.test.BaseTest
import ademar.study.trademe.core.test.Fixture.photo
import ademar.study.trademe.core.test.Fixture.photoValue
import ademar.study.trademe.core.test.containsJsonObject
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test

class PhotoTest : BaseTest() {

    private lateinit var adapter: JsonAdapter<Photo>

    @Before
    override fun setUp() {
        super.setUp()
        adapter = Moshi.Builder()
                .add(ApplicationJsonAdapterFactory.INSTANCE)
                .build()
                .adapter(Photo::class.java)
    }

    @Test
    fun testParse() {
        val photo = adapter.fromJson(readJson("photo")) ?: throw IllegalStateException("Failed to parse")
        assertThat(photo.value).isEqualTo(photoValue())
    }

    @Test
    fun testSerialize() {
        val json = adapter.toJson(photo())
        assertThat(json).containsJsonObject("Value")
    }

}
