package ademar.study.trademe.core.repository

import ademar.study.trademe.core.injection.ApplicationJsonAdapterFactory
import ademar.study.trademe.core.repository.datasource.CategoryCloudRepository
import ademar.study.trademe.core.repository.datasource.CategoryMemoryRepository
import ademar.study.trademe.core.test.BaseTest
import ademar.study.trademe.core.test.Fixture.ROOT_CATEGORY_NUMBER
import ademar.study.trademe.core.test.Fixture.rootCategory
import com.nhaarman.mockito_kotlin.whenever
import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.mockwebserver.MockResponse
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mockito.Mock
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

class CategoryRepositoryTest : BaseTest() {

    private lateinit var mockRetrofit: Retrofit
    private lateinit var mockCategoryCloudRepository: CategoryCloudRepository

    private var nextCalls = 0
    private var errorCalled = false
    private var successCalled = false

    @Mock lateinit var mockCategoryMemoryRepository: CategoryMemoryRepository

    override fun setUp() {
        super.setUp()
        nextCalls = 0
        errorCalled = false
        successCalled = false
        mockWebServer.start()

        mockRetrofit = Retrofit.Builder()
                .baseUrl(mockWebServer.url(""))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(MoshiConverterFactory.create(Moshi.Builder()
                        .add(ApplicationJsonAdapterFactory.INSTANCE)
                        .build()))
                .client(OkHttpClient.Builder()
                        .addInterceptor(HttpLoggingInterceptor().apply {
                            level = HttpLoggingInterceptor.Level.BODY
                        })
                        .build())
                .build()

        mockCategoryCloudRepository = mockRetrofit.create(CategoryCloudRepository::class.java)
    }

    override fun tearDown() {
        super.tearDown()
        mockWebServer.shutdown()
    }

    @Test
    fun testGetCategory_successService() {
        val mockResponse = MockResponse().setResponseCode(200)
                .setBody(readJson("category"))
        mockWebServer.enqueue(mockResponse)

        whenever(mockCategoryMemoryRepository.categories).thenReturn(hashMapOf())

        val repository = CategoryRepository(mockCategoryCloudRepository, mockCategoryMemoryRepository)

        repository.getCategory(ROOT_CATEGORY_NUMBER)
                .subscribe({
                    nextCalls++
                }, {
                    errorCalled = true
                }, {
                    successCalled = true
                })

        assertThat(nextCalls).isEqualTo(1)
        assertThat(errorCalled).isFalse()
        assertThat(successCalled).isTrue()
    }

    @Test
    fun testGetCategory_successCached_noUpdate() {
        val mockResponse = MockResponse().setResponseCode(200)
                .setBody(readJson("category"))
        mockWebServer.enqueue(mockResponse)

        whenever(mockCategoryMemoryRepository.categories).thenReturn(hashMapOf(
                ROOT_CATEGORY_NUMBER to rootCategory()
        ))

        val repository = CategoryRepository(mockCategoryCloudRepository, mockCategoryMemoryRepository)

        repository.getCategory(ROOT_CATEGORY_NUMBER)
                .subscribe({
                    nextCalls++
                }, {
                    errorCalled = true
                }, {
                    successCalled = true
                })

        assertThat(nextCalls).isEqualTo(1)
        assertThat(errorCalled).isFalse()
        assertThat(successCalled).isTrue()
    }

    @Test
    fun testGetCategory_successCached_update() {
        val mockResponse = MockResponse().setResponseCode(200)
                .setBody(readJson("category"))
        mockWebServer.enqueue(mockResponse)

        whenever(mockCategoryMemoryRepository.categories).thenReturn(hashMapOf(
                ROOT_CATEGORY_NUMBER to rootCategory().copy(subCategories = listOf())
        ))

        val repository = CategoryRepository(mockCategoryCloudRepository, mockCategoryMemoryRepository)

        repository.getCategory(ROOT_CATEGORY_NUMBER)
                .subscribe({
                    nextCalls++
                }, {
                    errorCalled = true
                }, {
                    successCalled = true
                })

        assertThat(nextCalls).isEqualTo(2)
        assertThat(errorCalled).isFalse()
        assertThat(successCalled).isTrue()
    }

    @Test
    fun testGetCategory_successCached_error() {
        val mockResponse = MockResponse().setResponseCode(0)
        mockWebServer.enqueue(mockResponse)

        whenever(mockCategoryMemoryRepository.categories).thenReturn(hashMapOf(
                ROOT_CATEGORY_NUMBER to rootCategory()
        ))

        val repository = CategoryRepository(mockCategoryCloudRepository, mockCategoryMemoryRepository)

        repository.getCategory(ROOT_CATEGORY_NUMBER)
                .subscribe({
                    nextCalls++
                }, {
                    errorCalled = true
                }, {
                    successCalled = true
                })

        assertThat(nextCalls).isEqualTo(1)
        assertThat(errorCalled).isFalse()
        assertThat(successCalled).isTrue()
    }

    @Test
    fun testGetCategory_successError() {
        val mockResponse = MockResponse().setResponseCode(0)
        mockWebServer.enqueue(mockResponse)

        whenever(mockCategoryMemoryRepository.categories).thenReturn(hashMapOf())

        val repository = CategoryRepository(mockCategoryCloudRepository, mockCategoryMemoryRepository)

        repository.getCategory(ROOT_CATEGORY_NUMBER)
                .subscribe({
                    nextCalls++
                }, { error ->
                    assertThat(error).isNotNull()
                    errorCalled = true
                }, {
                    successCalled = true
                })

        assertThat(nextCalls).isEqualTo(0)
        assertThat(errorCalled).isTrue()
        assertThat(successCalled).isFalse()
    }

}
