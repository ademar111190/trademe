package ademar.study.trademe.core.repository

import ademar.study.trademe.core.injection.ApplicationJsonAdapterFactory
import ademar.study.trademe.core.repository.datasource.SearchCloudRepository
import ademar.study.trademe.core.test.BaseTest
import ademar.study.trademe.core.test.Fixture.SEARCH_QUERY
import ademar.study.trademe.core.test.Fixture.rootCategory
import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.mockwebserver.MockResponse
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

class SearchRepositoryTest : BaseTest() {

    private lateinit var mockRetrofit: Retrofit
    private lateinit var mockSearchCloudRepository: SearchCloudRepository

    private var nextCalls = 0
    private var errorCalled = false
    private var successCalled = false

    override fun setUp() {
        super.setUp()
        nextCalls = 0
        errorCalled = false
        successCalled = false
        mockWebServer.start()

        mockRetrofit = Retrofit.Builder()
                .baseUrl(mockWebServer.url(""))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(MoshiConverterFactory.create(Moshi.Builder()
                        .add(ApplicationJsonAdapterFactory.INSTANCE)
                        .build()))
                .client(OkHttpClient.Builder()
                        .addInterceptor(HttpLoggingInterceptor().apply {
                            level = HttpLoggingInterceptor.Level.BODY
                        })
                        .build())
                .build()

        mockSearchCloudRepository = mockRetrofit.create(SearchCloudRepository::class.java)
    }

    override fun tearDown() {
        super.tearDown()
        mockWebServer.shutdown()
    }

    @Test
    fun testSearchCategory_success() {
        val mockResponse = MockResponse().setResponseCode(200)
                .setBody(readJson("search"))
        mockWebServer.enqueue(mockResponse)

        val repository = SearchRepository(mockSearchCloudRepository)

        repository.searchCategory(rootCategory())
                .subscribe({
                    nextCalls++
                }, {
                    errorCalled = true
                }, {
                    successCalled = true
                })

        assertThat(nextCalls).isEqualTo(1)
        assertThat(errorCalled).isFalse()
        assertThat(successCalled).isTrue()
    }

    @Test
    fun testSearchCategory_error() {
        val mockResponse = MockResponse().setResponseCode(0)
        mockWebServer.enqueue(mockResponse)

        val repository = SearchRepository(mockSearchCloudRepository)

        repository.searchCategory(rootCategory())
                .subscribe({
                    nextCalls++
                }, {
                    errorCalled = true
                }, {
                    successCalled = true
                })

        assertThat(nextCalls).isEqualTo(0)
        assertThat(errorCalled).isTrue()
        assertThat(successCalled).isFalse()
    }

    @Test
    fun testSearchQuery_success() {
        val mockResponse = MockResponse().setResponseCode(200)
                .setBody(readJson("search"))
        mockWebServer.enqueue(mockResponse)

        val repository = SearchRepository(mockSearchCloudRepository)

        repository.searchQuery(SEARCH_QUERY)
                .subscribe({
                    nextCalls++
                }, {
                    errorCalled = true
                }, {
                    successCalled = true
                })

        assertThat(nextCalls).isEqualTo(1)
        assertThat(errorCalled).isFalse()
        assertThat(successCalled).isTrue()
    }

    @Test
    fun testSearchQuery_error() {
        val mockResponse = MockResponse().setResponseCode(0)
        mockWebServer.enqueue(mockResponse)

        val repository = SearchRepository(mockSearchCloudRepository)

        repository.searchQuery(SEARCH_QUERY)
                .subscribe({
                    nextCalls++
                }, {
                    errorCalled = true
                }, {
                    successCalled = true
                })

        assertThat(nextCalls).isEqualTo(0)
        assertThat(errorCalled).isTrue()
        assertThat(successCalled).isFalse()
    }

    @Test
    fun testSearchQueryInCategory_success() {
        val mockResponse = MockResponse().setResponseCode(200)
                .setBody(readJson("search"))
        mockWebServer.enqueue(mockResponse)

        val repository = SearchRepository(mockSearchCloudRepository)

        repository.searchQueryInCategory(SEARCH_QUERY, rootCategory())
                .subscribe({
                    nextCalls++
                }, {
                    errorCalled = true
                }, {
                    successCalled = true
                })

        assertThat(nextCalls).isEqualTo(1)
        assertThat(errorCalled).isFalse()
        assertThat(successCalled).isTrue()
    }

    @Test
    fun testSearchQueryInCategory_error() {
        val mockResponse = MockResponse().setResponseCode(0)
        mockWebServer.enqueue(mockResponse)

        val repository = SearchRepository(mockSearchCloudRepository)

        repository.searchQueryInCategory(SEARCH_QUERY, rootCategory())
                .subscribe({
                    nextCalls++
                }, {
                    errorCalled = true
                }, {
                    successCalled = true
                })

        assertThat(nextCalls).isEqualTo(0)
        assertThat(errorCalled).isTrue()
        assertThat(successCalled).isFalse()
    }

}
