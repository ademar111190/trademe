package ademar.study.trademe.core.model

import ademar.study.trademe.core.injection.ApplicationJsonAdapterFactory
import ademar.study.trademe.core.test.BaseTest
import ademar.study.trademe.core.test.Fixture.ALFA_ROMEO_CATEGORY_AREA
import ademar.study.trademe.core.test.Fixture.ALFA_ROMEO_CATEGORY_COUNT
import ademar.study.trademe.core.test.Fixture.ALFA_ROMEO_CATEGORY_LEAF
import ademar.study.trademe.core.test.Fixture.ALFA_ROMEO_CATEGORY_NAME
import ademar.study.trademe.core.test.Fixture.ALFA_ROMEO_CATEGORY_NUMBER
import ademar.study.trademe.core.test.Fixture.ALFA_ROMEO_CATEGORY_PATH
import ademar.study.trademe.core.test.Fixture.CARS_CATEGORY_AREA
import ademar.study.trademe.core.test.Fixture.CARS_CATEGORY_COUNT
import ademar.study.trademe.core.test.Fixture.CARS_CATEGORY_LEAF
import ademar.study.trademe.core.test.Fixture.CARS_CATEGORY_NAME
import ademar.study.trademe.core.test.Fixture.CARS_CATEGORY_NUMBER
import ademar.study.trademe.core.test.Fixture.CARS_CATEGORY_PATH
import ademar.study.trademe.core.test.Fixture.MOTORS_CATEGORY_AREA
import ademar.study.trademe.core.test.Fixture.MOTORS_CATEGORY_COUNT
import ademar.study.trademe.core.test.Fixture.MOTORS_CATEGORY_LEAF
import ademar.study.trademe.core.test.Fixture.MOTORS_CATEGORY_NAME
import ademar.study.trademe.core.test.Fixture.MOTORS_CATEGORY_NUMBER
import ademar.study.trademe.core.test.Fixture.MOTORS_CATEGORY_PATH
import ademar.study.trademe.core.test.Fixture.ROOT_CATEGORY_AREA
import ademar.study.trademe.core.test.Fixture.ROOT_CATEGORY_COUNT
import ademar.study.trademe.core.test.Fixture.ROOT_CATEGORY_LEAF
import ademar.study.trademe.core.test.Fixture.ROOT_CATEGORY_NAME
import ademar.study.trademe.core.test.Fixture.ROOT_CATEGORY_NUMBER
import ademar.study.trademe.core.test.Fixture.ROOT_CATEGORY_PATH
import ademar.study.trademe.core.test.Fixture.alfaRomeoCategory
import ademar.study.trademe.core.test.Fixture.carsCategory
import ademar.study.trademe.core.test.Fixture.motorsCategory
import ademar.study.trademe.core.test.Fixture.rootCategory
import ademar.study.trademe.core.test.containsJson
import ademar.study.trademe.core.test.containsJsonList
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test

class CategoryTest : BaseTest() {

    private lateinit var adapter: JsonAdapter<Category>

    @Before
    override fun setUp() {
        super.setUp()
        adapter = Moshi.Builder()
                .add(ApplicationJsonAdapterFactory.INSTANCE)
                .build()
                .adapter(Category::class.java)
    }

    @Test
    fun testParse() {
        val rootCategory = adapter.fromJson(readJson("category")) ?: throw IllegalStateException("Failed to parse")
        assertThat(rootCategory.name).isEqualTo(ROOT_CATEGORY_NAME)
        assertThat(rootCategory.number).isEqualTo(ROOT_CATEGORY_NUMBER)
        assertThat(rootCategory.path).isEqualTo(ROOT_CATEGORY_PATH)
        assertThat(rootCategory.leaf).isEqualTo(ROOT_CATEGORY_LEAF)
        assertThat(rootCategory.area).isEqualTo(ROOT_CATEGORY_AREA)
        assertThat(rootCategory.count).isEqualTo(ROOT_CATEGORY_COUNT)
        assertThat(rootCategory.subCategories.size).isEqualTo(1)

        val motorsCategory = rootCategory.subCategories.first()
        assertThat(motorsCategory.name).isEqualTo(MOTORS_CATEGORY_NAME)
        assertThat(motorsCategory.number).isEqualTo(MOTORS_CATEGORY_NUMBER)
        assertThat(motorsCategory.path).isEqualTo(MOTORS_CATEGORY_PATH)
        assertThat(motorsCategory.leaf).isEqualTo(MOTORS_CATEGORY_LEAF)
        assertThat(motorsCategory.area).isEqualTo(MOTORS_CATEGORY_AREA)
        assertThat(motorsCategory.count).isEqualTo(MOTORS_CATEGORY_COUNT)
        assertThat(motorsCategory.subCategories.size).isEqualTo(1)

        val carsCategory = motorsCategory.subCategories.first()
        assertThat(carsCategory.name).isEqualTo(CARS_CATEGORY_NAME)
        assertThat(carsCategory.number).isEqualTo(CARS_CATEGORY_NUMBER)
        assertThat(carsCategory.path).isEqualTo(CARS_CATEGORY_PATH)
        assertThat(carsCategory.leaf).isEqualTo(CARS_CATEGORY_LEAF)
        assertThat(carsCategory.area).isEqualTo(CARS_CATEGORY_AREA)
        assertThat(carsCategory.count).isEqualTo(CARS_CATEGORY_COUNT)
        assertThat(carsCategory.subCategories.size).isEqualTo(1)

        val alfaRomeoCategory = carsCategory.subCategories.first()
        assertThat(alfaRomeoCategory.name).isEqualTo(ALFA_ROMEO_CATEGORY_NAME)
        assertThat(alfaRomeoCategory.number).isEqualTo(ALFA_ROMEO_CATEGORY_NUMBER)
        assertThat(alfaRomeoCategory.path).isEqualTo(ALFA_ROMEO_CATEGORY_PATH)
        assertThat(alfaRomeoCategory.leaf).isEqualTo(ALFA_ROMEO_CATEGORY_LEAF)
        assertThat(alfaRomeoCategory.area).isEqualTo(ALFA_ROMEO_CATEGORY_AREA)
        assertThat(alfaRomeoCategory.count).isEqualTo(ALFA_ROMEO_CATEGORY_COUNT)
        assertThat(alfaRomeoCategory.subCategories).isEmpty()
    }

    @Test
    fun testSerialize() {
        var json = adapter.toJson(rootCategory())
        assertThat(json).containsJson("Name", ROOT_CATEGORY_NAME)
        assertThat(json).containsJson("Number", ROOT_CATEGORY_NUMBER)
        assertThat(json).containsJson("Path", ROOT_CATEGORY_PATH)
        assertThat(json).containsJson("IsLeaf", ROOT_CATEGORY_LEAF)
        assertThat(json).containsJson("AreaOfBusiness", ROOT_CATEGORY_AREA)
        assertThat(json).containsJson("Count", ROOT_CATEGORY_COUNT)
        assertThat(json).containsJsonList("Subcategories")

        json = adapter.toJson(motorsCategory())
        assertThat(json).containsJson("Name", MOTORS_CATEGORY_NAME)
        assertThat(json).containsJson("Number", MOTORS_CATEGORY_NUMBER)
        assertThat(json).containsJson("Path", MOTORS_CATEGORY_PATH)
        assertThat(json).containsJson("IsLeaf", MOTORS_CATEGORY_LEAF)
        assertThat(json).containsJson("AreaOfBusiness", MOTORS_CATEGORY_AREA)
        assertThat(json).containsJson("Count", MOTORS_CATEGORY_COUNT)
        assertThat(json).containsJsonList("Subcategories")

        json = adapter.toJson(carsCategory())
        assertThat(json).containsJson("Name", CARS_CATEGORY_NAME)
        assertThat(json).containsJson("Number", CARS_CATEGORY_NUMBER)
        assertThat(json).containsJson("Path", CARS_CATEGORY_PATH)
        assertThat(json).containsJson("IsLeaf", CARS_CATEGORY_LEAF)
        assertThat(json).containsJson("AreaOfBusiness", CARS_CATEGORY_AREA)
        assertThat(json).containsJson("Count", CARS_CATEGORY_COUNT)
        assertThat(json).containsJsonList("Subcategories")

        json = adapter.toJson(alfaRomeoCategory())
        assertThat(json).containsJson("Name", ALFA_ROMEO_CATEGORY_NAME)
        assertThat(json).containsJson("Number", ALFA_ROMEO_CATEGORY_NUMBER)
        assertThat(json).containsJson("Path", ALFA_ROMEO_CATEGORY_PATH)
        assertThat(json).containsJson("IsLeaf", ALFA_ROMEO_CATEGORY_LEAF)
        assertThat(json).containsJson("AreaOfBusiness", ALFA_ROMEO_CATEGORY_AREA)
        assertThat(json).containsJson("Count", ALFA_ROMEO_CATEGORY_COUNT)
        assertThat(json).containsJsonList("Subcategories")
    }

}
