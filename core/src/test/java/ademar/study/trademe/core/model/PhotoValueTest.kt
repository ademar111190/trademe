package ademar.study.trademe.core.model

import ademar.study.trademe.core.injection.ApplicationJsonAdapterFactory
import ademar.study.trademe.core.test.BaseTest
import ademar.study.trademe.core.test.Fixture.PHOTO_VALUE_FULL_SIZE
import ademar.study.trademe.core.test.Fixture.PHOTO_VALUE_GALLERY
import ademar.study.trademe.core.test.Fixture.PHOTO_VALUE_LARGE
import ademar.study.trademe.core.test.Fixture.PHOTO_VALUE_LIST
import ademar.study.trademe.core.test.Fixture.PHOTO_VALUE_MEDIUM
import ademar.study.trademe.core.test.Fixture.PHOTO_VALUE_THUMBNAIL
import ademar.study.trademe.core.test.Fixture.photoValue
import ademar.study.trademe.core.test.containsJson
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test

class PhotoValueTest : BaseTest() {

    private lateinit var adapter: JsonAdapter<PhotoValue>

    @Before
    override fun setUp() {
        super.setUp()
        adapter = Moshi.Builder()
                .add(ApplicationJsonAdapterFactory.INSTANCE)
                .build()
                .adapter(PhotoValue::class.java)
    }

    @Test
    fun testParse() {
        val photoValue = adapter.fromJson(readJson("photoValue")) ?: throw IllegalStateException("Failed to parse")
        assertThat(photoValue.thumbnail).isEqualTo(PHOTO_VALUE_THUMBNAIL)
        assertThat(photoValue.list).isEqualTo(PHOTO_VALUE_LIST)
        assertThat(photoValue.medium).isEqualTo(PHOTO_VALUE_MEDIUM)
        assertThat(photoValue.gallery).isEqualTo(PHOTO_VALUE_GALLERY)
        assertThat(photoValue.large).isEqualTo(PHOTO_VALUE_LARGE)
        assertThat(photoValue.fullSize).isEqualTo(PHOTO_VALUE_FULL_SIZE)
    }

    @Test
    fun testSerialize() {
        val json = adapter.toJson(photoValue())
        assertThat(json).containsJson("Thumbnail", PHOTO_VALUE_THUMBNAIL)
        assertThat(json).containsJson("List", PHOTO_VALUE_LIST)
        assertThat(json).containsJson("Medium", PHOTO_VALUE_MEDIUM)
        assertThat(json).containsJson("Gallery", PHOTO_VALUE_GALLERY)
        assertThat(json).containsJson("Large", PHOTO_VALUE_LARGE)
        assertThat(json).containsJson("FullSize", PHOTO_VALUE_FULL_SIZE)
    }

}
