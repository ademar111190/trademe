package ademar.study.trademe.core.model

import ademar.study.trademe.core.test.BaseTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.util.*

class AreaTest : BaseTest() {

    @Test
    fun testGetArea() {
        assertThat(getArea(0)).isEqualTo(Area.ALL)
        assertThat(getArea(1)).isEqualTo(Area.MARKETPLACE)
        assertThat(getArea(2)).isEqualTo(Area.PROPERTY)
        assertThat(getArea(3)).isEqualTo(Area.MOTORS)
        assertThat(getArea(4)).isEqualTo(Area.JOBS)
        assertThat(getArea(5)).isEqualTo(Area.SERVICES)

        var notValidId: Int
        do {
            notValidId = Random().nextInt()
        } while (notValidId in 0..5)
        assertThat(getArea(notValidId)).isEqualTo(Area.ALL)
    }

}
