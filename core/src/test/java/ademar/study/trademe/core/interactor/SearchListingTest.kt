package ademar.study.trademe.core.interactor

import ademar.study.trademe.core.repository.SearchRepository
import ademar.study.trademe.core.test.BaseTest
import ademar.study.trademe.core.test.Fixture
import ademar.study.trademe.core.test.Fixture.SEARCH_QUERY
import ademar.study.trademe.core.test.Fixture.listing
import ademar.study.trademe.core.test.Fixture.rootCategory
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Observable
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions

class SearchListingTest : BaseTest() {

    @Mock lateinit var mockSearchRepository: SearchRepository

    @Test
    fun testExecute_queryAndCategory_success() {
        val useCase = SearchListing(mockSearchRepository)

        whenever(mockSearchRepository.searchQueryInCategory(SEARCH_QUERY, rootCategory())).thenReturn(Observable.just(listing()))

        useCase.execute(SEARCH_QUERY, rootCategory())
                .test()
                .assertResult(listing())
                .assertNoErrors()

        verify(mockSearchRepository).searchQueryInCategory(SEARCH_QUERY, rootCategory())
        verifyNoMoreInteractions(mockSearchRepository)
    }

    @Test
    fun testExecute_queryAndCategory_error() {
        val useCase = SearchListing(mockSearchRepository)
        val mockError = Fixture.error()

        whenever(mockSearchRepository.searchQueryInCategory(SEARCH_QUERY, rootCategory())).thenReturn(Observable.error(mockError))

        useCase.execute(SEARCH_QUERY, rootCategory())
                .test()
                .assertError(mockError)

        verify(mockSearchRepository).searchQueryInCategory(SEARCH_QUERY, rootCategory())
        verifyNoMoreInteractions(mockSearchRepository)
    }

    @Test
    fun testExecute_query_success() {
        val useCase = SearchListing(mockSearchRepository)

        whenever(mockSearchRepository.searchQuery(SEARCH_QUERY)).thenReturn(Observable.just(listing()))

        useCase.execute(SEARCH_QUERY)
                .test()
                .assertResult(listing())
                .assertNoErrors()

        verify(mockSearchRepository).searchQuery(SEARCH_QUERY)
        verifyNoMoreInteractions(mockSearchRepository)
    }

    @Test
    fun testExecute_query_error() {
        val useCase = SearchListing(mockSearchRepository)
        val mockError = Fixture.error()

        whenever(mockSearchRepository.searchQuery(SEARCH_QUERY)).thenReturn(Observable.error(mockError))

        useCase.execute(SEARCH_QUERY)
                .test()
                .assertError(mockError)

        verify(mockSearchRepository).searchQuery(SEARCH_QUERY)
        verifyNoMoreInteractions(mockSearchRepository)
    }

    @Test
    fun testExecute_category_success() {
        val useCase = SearchListing(mockSearchRepository)

        whenever(mockSearchRepository.searchCategory(rootCategory())).thenReturn(Observable.just(listing()))

        useCase.execute(category = rootCategory())
                .test()
                .assertResult(listing())
                .assertNoErrors()

        verify(mockSearchRepository).searchCategory(rootCategory())
        verifyNoMoreInteractions(mockSearchRepository)
    }

    @Test
    fun testExecute_category_error() {
        val useCase = SearchListing(mockSearchRepository)
        val mockError = Fixture.error()

        whenever(mockSearchRepository.searchCategory(rootCategory())).thenReturn(Observable.error(mockError))

        useCase.execute(category = rootCategory())
                .test()
                .assertError(mockError)

        verify(mockSearchRepository).searchCategory(rootCategory())
        verifyNoMoreInteractions(mockSearchRepository)
    }

    @Test
    fun testExecute_noArgs() {
        val useCase = SearchListing(mockSearchRepository)

        useCase.execute()
                .test()
                .assertResult()
                .assertNoErrors()
    }

}
