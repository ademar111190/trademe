package ademar.study.trademe.core.model

import ademar.study.trademe.core.injection.ApplicationJsonAdapterFactory
import ademar.study.trademe.core.test.BaseTest
import ademar.study.trademe.core.test.Fixture.LISTING_ID
import ademar.study.trademe.core.test.Fixture.listing
import ademar.study.trademe.core.test.containsJson
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test

class ListingTest : BaseTest() {

    private lateinit var adapter: JsonAdapter<Listing>

    @Before
    override fun setUp() {
        super.setUp()
        adapter = Moshi.Builder()
                .add(ApplicationJsonAdapterFactory.INSTANCE)
                .build()
                .adapter(Listing::class.java)
    }

    @Test
    fun testParse() {
        val listing = adapter.fromJson(readJson("listing")) ?: throw IllegalStateException("Failed to parse")
        assertThat(listing.id).isEqualTo(LISTING_ID)
    }

    @Test
    fun testSerialize() {
        val json = adapter.toJson(listing())
        assertThat(json).containsJson("ListingId", LISTING_ID)
    }

}
