package ademar.study.trademe.core.interactor

import ademar.study.trademe.core.repository.CategoryRepository
import ademar.study.trademe.core.test.BaseTest
import ademar.study.trademe.core.test.Fixture
import ademar.study.trademe.core.test.Fixture.CATEGORY_NUMBER
import ademar.study.trademe.core.test.Fixture.motorsCategory
import ademar.study.trademe.core.test.Fixture.rootCategory
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Observable
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions

class GetCategoryTest : BaseTest() {

    @Mock lateinit var mockCategoryRepository: CategoryRepository

    @Test
    fun testExecute_success() {
        val useCase = GetCategory(mockCategoryRepository)

        whenever(mockCategoryRepository.getCategory(CATEGORY_NUMBER)).thenReturn(Observable.just(rootCategory()))

        useCase.execute()
                .test()
                .assertResult(motorsCategory())
                .assertNoErrors()

        verify(mockCategoryRepository).getCategory(CATEGORY_NUMBER)
        verifyNoMoreInteractions(mockCategoryRepository)
    }

    @Test
    fun testExecute_success_customNumber() {
        val useCase = GetCategory(mockCategoryRepository)

        whenever(mockCategoryRepository.getCategory("12345")).thenReturn(Observable.just(rootCategory()))

        useCase.execute("12345")
                .test()
                .assertResult(motorsCategory())
                .assertNoErrors()

        verify(mockCategoryRepository).getCategory("12345")
        verifyNoMoreInteractions(mockCategoryRepository)
    }

    @Test
    fun testExecute_success_malFormattedNumber() {
        val useCase = GetCategory(mockCategoryRepository)

        whenever(mockCategoryRepository.getCategory(CATEGORY_NUMBER)).thenReturn(Observable.just(rootCategory()))

        useCase.execute(" 0 ")
                .test()
                .assertResult(motorsCategory())
                .assertNoErrors()

        verify(mockCategoryRepository).getCategory(CATEGORY_NUMBER)
        verifyNoMoreInteractions(mockCategoryRepository)
    }

    @Test
    fun testExecute_success_nullNumber() {
        val useCase = GetCategory(mockCategoryRepository)

        whenever(mockCategoryRepository.getCategory(CATEGORY_NUMBER)).thenReturn(Observable.just(rootCategory()))

        useCase.execute(null)
                .test()
                .assertResult(motorsCategory())
                .assertNoErrors()

        verify(mockCategoryRepository).getCategory(CATEGORY_NUMBER)
        verifyNoMoreInteractions(mockCategoryRepository)
    }

    @Test
    fun testExecute_error() {
        val useCase = GetCategory(mockCategoryRepository)
        val mockError = Fixture.error()

        whenever(mockCategoryRepository.getCategory(CATEGORY_NUMBER)).thenReturn(Observable.error(mockError))

        useCase.execute()
                .test()
                .assertError(mockError)

        verify(mockCategoryRepository).getCategory(CATEGORY_NUMBER)
        verifyNoMoreInteractions(mockCategoryRepository)
    }

}
