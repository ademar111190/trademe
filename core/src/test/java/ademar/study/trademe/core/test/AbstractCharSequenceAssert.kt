package ademar.study.trademe.core.test

import org.assertj.core.api.AbstractCharSequenceAssert

fun AbstractCharSequenceAssert<*, String>.containsJsonObject(key: String)
        : AbstractCharSequenceAssert<*, *> = contains("\"$key\":{")

fun AbstractCharSequenceAssert<*, String>.containsJsonList(key: String)
        : AbstractCharSequenceAssert<*, *> = contains("\"$key\":[")

fun AbstractCharSequenceAssert<*, String>.containsJson(key: String, value: String)
        : AbstractCharSequenceAssert<*, *> = contains("\"$key\":\"$value\"")

fun AbstractCharSequenceAssert<*, String>.containsJson(key: String, value: Boolean)
        : AbstractCharSequenceAssert<*, *> = contains("\"$key\":$value")

fun AbstractCharSequenceAssert<*, String>.containsJson(key: String, value: Int)
        : AbstractCharSequenceAssert<*, *> = contains("\"$key\":$value")

fun AbstractCharSequenceAssert<*, String>.containsJson(key: String, value: Long)
        : AbstractCharSequenceAssert<*, *> = contains("\"$key\":$value")
