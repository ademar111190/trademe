package ademar.study.trademe.core.interactor

import ademar.study.trademe.core.repository.ListingRepository
import ademar.study.trademe.core.test.BaseTest
import ademar.study.trademe.core.test.Fixture
import ademar.study.trademe.core.test.Fixture.LISTING_ID
import ademar.study.trademe.core.test.Fixture.listing
import ademar.study.trademe.core.test.Fixture.listingDetail
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Observable
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions

class GetListingDetailTest : BaseTest() {

    @Mock lateinit var mockListingRepository: ListingRepository

    @Test
    fun testExecute_success() {
        val useCase = GetListingDetail(mockListingRepository)

        whenever(mockListingRepository.getListingDetail(LISTING_ID)).thenReturn(Observable.just(listingDetail()))

        useCase.execute(listing())
                .test()
                .assertResult(listingDetail())
                .assertNoErrors()

        verify(mockListingRepository).getListingDetail(LISTING_ID)
        verifyNoMoreInteractions(mockListingRepository)
    }

    @Test
    fun testExecute_error() {
        val useCase = GetListingDetail(mockListingRepository)
        val mockError = Fixture.error()

        whenever(mockListingRepository.getListingDetail(LISTING_ID)).thenReturn(Observable.error(mockError))

        useCase.execute(listing())
                .test()
                .assertError(mockError)

        verify(mockListingRepository).getListingDetail(LISTING_ID)
        verifyNoMoreInteractions(mockListingRepository)
    }

}
