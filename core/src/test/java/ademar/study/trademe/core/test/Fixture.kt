package ademar.study.trademe.core.test

import ademar.study.trademe.core.model.*

object Fixture {

    const val ALFA_ROMEO_CATEGORY_AREA = 3
    const val ALFA_ROMEO_CATEGORY_COUNT = 0
    const val ALFA_ROMEO_CATEGORY_LEAF = true
    const val ALFA_ROMEO_CATEGORY_NAME = "Alfa Romeo"
    const val ALFA_ROMEO_CATEGORY_NUMBER = "0001-0268-0269-"
    const val ALFA_ROMEO_CATEGORY_PATH = "/Trade-Me-Motors/Cars/Alfa-Romeo"

    const val CARS_CATEGORY_AREA = 0
    const val CARS_CATEGORY_COUNT = 2
    const val CARS_CATEGORY_LEAF = false
    const val CARS_CATEGORY_NAME = "Cars"
    const val CARS_CATEGORY_NUMBER = "0001-0268-"
    const val CARS_CATEGORY_PATH = "/Trade-Me-Motors/Cars"

    const val CATEGORY_NUMBER = "0"

    const val LISTING_DETAIL_CATEGORY_ID = "0001-0268-0310-"
    const val LISTING_DETAIL_ID = 6612200L
    const val LISTING_DETAIL_TITLE = "Mazda 626 1997"

    const val LISTING_ID = 6612200L

    const val MESSAGE = "Hello World!"

    const val MOTORS_CATEGORY_AREA = 0
    const val MOTORS_CATEGORY_COUNT = 2
    const val MOTORS_CATEGORY_LEAF = false
    const val MOTORS_CATEGORY_NAME = "Trade Me Motors"
    const val MOTORS_CATEGORY_NUMBER = "0001-"
    const val MOTORS_CATEGORY_PATH = "/Trade-Me-Motors"

    const val PHOTO_VALUE_FULL_SIZE = "https://images.tmsandbox.co.nz/photoserver/full/3529603.jpg"
    const val PHOTO_VALUE_GALLERY = "https://images.tmsandbox.co.nz/photoserver/gv/3529603.jpg"
    const val PHOTO_VALUE_LARGE = "https://images.tmsandbox.co.nz/photoserver/tq/3529603.jpg"
    const val PHOTO_VALUE_LIST = "https://images.tmsandbox.co.nz/photoserver/lv2/3529603.jpg"
    const val PHOTO_VALUE_MEDIUM = "https://images.tmsandbox.co.nz/photoserver/med/3529603.jpg"
    const val PHOTO_VALUE_THUMBNAIL = "https://images.tmsandbox.co.nz/photoserver/thumb/3529603.jpg"

    const val ROOT_CATEGORY_AREA = 0
    const val ROOT_CATEGORY_COUNT = 14281
    const val ROOT_CATEGORY_LEAF = false
    const val ROOT_CATEGORY_NAME = "Root"
    const val ROOT_CATEGORY_NUMBER = ""
    const val ROOT_CATEGORY_PATH = ""

    const val SEARCH_QUERY = "A query"

    fun error() = Error(MESSAGE)

    fun rootCategory() = Category(
            ROOT_CATEGORY_NAME,
            ROOT_CATEGORY_NUMBER,
            ROOT_CATEGORY_PATH,
            ROOT_CATEGORY_LEAF,
            ROOT_CATEGORY_AREA,
            ROOT_CATEGORY_COUNT,
            listOf(motorsCategory()))

    fun motorsCategory() = Category(
            MOTORS_CATEGORY_NAME,
            MOTORS_CATEGORY_NUMBER,
            MOTORS_CATEGORY_PATH,
            MOTORS_CATEGORY_LEAF,
            MOTORS_CATEGORY_AREA,
            MOTORS_CATEGORY_COUNT,
            listOf(carsCategory()))

    fun carsCategory() = Category(
            CARS_CATEGORY_NAME,
            CARS_CATEGORY_NUMBER,
            CARS_CATEGORY_PATH,
            CARS_CATEGORY_LEAF,
            CARS_CATEGORY_AREA,
            CARS_CATEGORY_COUNT,
            listOf(alfaRomeoCategory()))

    fun alfaRomeoCategory() = Category(
            ALFA_ROMEO_CATEGORY_NAME,
            ALFA_ROMEO_CATEGORY_NUMBER,
            ALFA_ROMEO_CATEGORY_PATH,
            ALFA_ROMEO_CATEGORY_LEAF,
            ALFA_ROMEO_CATEGORY_AREA,
            ALFA_ROMEO_CATEGORY_COUNT,
            listOf())

    fun photo() = Photo(photoValue())

    fun photoValue() = PhotoValue(PHOTO_VALUE_THUMBNAIL,
            PHOTO_VALUE_LIST,
            PHOTO_VALUE_MEDIUM,
            PHOTO_VALUE_GALLERY,
            PHOTO_VALUE_LARGE,
            PHOTO_VALUE_FULL_SIZE)

    fun listing() = Listing(LISTING_ID)

    fun listingDetail() = ListingDetail(
            LISTING_DETAIL_ID,
            LISTING_DETAIL_TITLE,
            LISTING_DETAIL_CATEGORY_ID,
            listOf(photo()))

    fun search() = Search(listOf(listing()))

}
