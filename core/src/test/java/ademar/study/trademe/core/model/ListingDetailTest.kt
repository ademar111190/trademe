package ademar.study.trademe.core.model

import ademar.study.trademe.core.injection.ApplicationJsonAdapterFactory
import ademar.study.trademe.core.test.BaseTest
import ademar.study.trademe.core.test.Fixture.LISTING_DETAIL_CATEGORY_ID
import ademar.study.trademe.core.test.Fixture.LISTING_DETAIL_ID
import ademar.study.trademe.core.test.Fixture.LISTING_DETAIL_TITLE
import ademar.study.trademe.core.test.Fixture.listingDetail
import ademar.study.trademe.core.test.Fixture.photo
import ademar.study.trademe.core.test.containsJson
import ademar.study.trademe.core.test.containsJsonList
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test

class ListingDetailTest : BaseTest() {

    private lateinit var adapter: JsonAdapter<ListingDetail>

    @Before
    override fun setUp() {
        super.setUp()
        adapter = Moshi.Builder()
                .add(ApplicationJsonAdapterFactory.INSTANCE)
                .build()
                .adapter(ListingDetail::class.java)
    }

    @Test
    fun testParse() {
        val listingDetail = adapter.fromJson(readJson("listingDetail")) ?: throw IllegalStateException("Failed to parse")
        assertThat(listingDetail.id).isEqualTo(LISTING_DETAIL_ID)
        assertThat(listingDetail.title).isEqualTo(LISTING_DETAIL_TITLE)
        assertThat(listingDetail.categoryId).isEqualTo(LISTING_DETAIL_CATEGORY_ID)
        assertThat(listingDetail.photos).isEqualTo(listOf(photo()))
    }

    @Test
    fun testSerialize() {
        val json = adapter.toJson(listingDetail())
        assertThat(json).containsJson("ListingId", LISTING_DETAIL_ID)
        assertThat(json).containsJson("Title", LISTING_DETAIL_TITLE)
        assertThat(json).containsJson("Category", LISTING_DETAIL_CATEGORY_ID)
        assertThat(json).containsJsonList("Photos")
    }

}
