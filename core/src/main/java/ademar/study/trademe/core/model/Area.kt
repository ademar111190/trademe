package ademar.study.trademe.core.model

enum class Area(

        val id: Int

) {

    ALL(0),
    MARKETPLACE(1),
    PROPERTY(2),
    MOTORS(3),
    JOBS(4),
    SERVICES(5);

}

fun getArea(id: Int) = Area.values().firstOrNull { it.id == id } ?: Area.ALL
