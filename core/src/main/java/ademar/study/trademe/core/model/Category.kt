package ademar.study.trademe.core.model

import ademar.study.trademe.core.injection.JsonDefaultValueList
import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import se.ansman.kotshi.JsonDefaultValueBoolean
import se.ansman.kotshi.JsonDefaultValueInt
import se.ansman.kotshi.JsonDefaultValueString
import se.ansman.kotshi.JsonSerializable

@JsonSerializable
@Parcelize
data class Category(

        @Json(name = "Name")
        val name: String,

        @Json(name = "Number")
        val number: String,

        @Json(name = "Path")
        @JsonDefaultValueString(value = "")
        val path: String,

        @Json(name = "IsLeaf")
        @JsonDefaultValueBoolean(value = false)
        val leaf: Boolean,

        @Json(name = "AreaOfBusiness")
        @JsonDefaultValueInt(value = 0)
        val area: Int,

        @Json(name = "Count")
        @JsonDefaultValueInt(value = 0)
        val count: Int,

        @Json(name = "Subcategories")
        @JsonDefaultValueList
        val subCategories: List<Category>

) : Parcelable
