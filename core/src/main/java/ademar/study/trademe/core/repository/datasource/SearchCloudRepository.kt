package ademar.study.trademe.core.repository.datasource

import ademar.study.trademe.core.model.Search
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface SearchCloudRepository {

    @GET("/v1/Search/General.json")
    fun search(
            @Query("category") categoryNumber: String = "",
            @Query("search_string") query: String = "",
            @Query("rows") limit: Int = 20,
            @Query("buy") buy: String = "All",
            @Query("clearance") clearance: String = "All",
            @Query("condition") condition: String = "All",
            @Query("expired") expired: Boolean = false,
            @Query("listed_as") listedAs: String = "All",
            @Query("pay") pay: String = "All",
            @Query("photo_size") photoSize: String = "Thumbnail",
            @Query("return_did_you_mean") returnDidYouMean: Boolean = false,
            @Query("return_metadata") returnMetadata: Boolean = false,
            @Query("shipping_method") shippingMethod: String = "All",
            @Query("sort_order") sort_order: String = "TitleAsc"
    ): Observable<Search>

}
