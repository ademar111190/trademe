package ademar.study.trademe.core.interactor

import ademar.study.trademe.core.model.Listing
import ademar.study.trademe.core.repository.ListingRepository
import javax.inject.Inject

class GetListingDetail @Inject constructor(

        private val repository: ListingRepository

) {

    fun execute(listing: Listing) = repository.getListingDetail(listing.id)

}
