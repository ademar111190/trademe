package ademar.study.trademe.core.interactor

import ademar.study.trademe.core.model.Category
import ademar.study.trademe.core.repository.CategoryRepository
import io.reactivex.Observable
import javax.inject.Inject

class GetCategory @Inject constructor(

        private val repository: CategoryRepository

) {

    fun execute(number: String? = null): Observable<Category> = repository
            .getCategory(if (number == null || number.isBlank()) "0" else number.trim())
            .flatMapIterable { it.subCategories }
            .distinct()

}
