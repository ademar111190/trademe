package ademar.study.trademe.core.repository.datasource

import ademar.study.trademe.core.model.ListingDetail
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ListingMemoryRepository @Inject constructor() {

    var listings: HashMap<Long, ListingDetail> = hashMapOf()

}
