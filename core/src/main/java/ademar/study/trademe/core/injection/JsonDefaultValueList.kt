package ademar.study.trademe.core.injection

import se.ansman.kotshi.JsonDefaultValue

@Target(AnnotationTarget.VALUE_PARAMETER,
        AnnotationTarget.FUNCTION,
        AnnotationTarget.CONSTRUCTOR,
        AnnotationTarget.FIELD,
        AnnotationTarget.PROPERTY_GETTER)
@Retention(AnnotationRetention.SOURCE)
@JsonDefaultValue
annotation class JsonDefaultValueList

@JsonDefaultValueList
fun <T> defaultJsonDefaultValueList() = listOf<T>()
