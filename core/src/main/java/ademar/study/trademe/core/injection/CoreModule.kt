package ademar.study.trademe.core.injection

import ademar.study.trademe.core.repository.datasource.CategoryCloudRepository
import ademar.study.trademe.core.repository.datasource.ListingCloudRepository
import ademar.study.trademe.core.repository.datasource.SearchCloudRepository
import android.content.Context
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
class CoreModule(

        private val context: Context,
        private val apiUrl: String

) {

    @Provides
    fun provideContext() = context

    @Provides
    @Singleton
    fun provideHttpLoggingInterceptor() = HttpLoggingInterceptor()

    @Provides
    @Singleton
    fun provideOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient = OkHttpClient.Builder()
            .addInterceptor(httpLoggingInterceptor)
            .build()

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
            .baseUrl(apiUrl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create(Moshi.Builder()
                    .add(ApplicationJsonAdapterFactory.INSTANCE)
                    .build()))
            .client(okHttpClient)
            .build()

    @Provides
    fun provideCategoryCloudRepository(retrofit: Retrofit): CategoryCloudRepository =
            retrofit.create(CategoryCloudRepository::class.java)

    @Provides
    fun provideListingCloudRepository(retrofit: Retrofit): ListingCloudRepository =
            retrofit.create(ListingCloudRepository::class.java)

    @Provides
    fun provideSearchCloudRepository(retrofit: Retrofit): SearchCloudRepository =
            retrofit.create(SearchCloudRepository::class.java)

}
