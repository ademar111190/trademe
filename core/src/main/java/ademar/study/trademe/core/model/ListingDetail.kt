package ademar.study.trademe.core.model

import ademar.study.trademe.core.injection.JsonDefaultValueList
import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import se.ansman.kotshi.JsonSerializable

@JsonSerializable
@Parcelize
data class ListingDetail(

        @Json(name = "ListingId")
        val id: Long,

        @Json(name = "Title")
        val title: String,

        @Json(name = "Category")
        val categoryId: String,

        @Json(name = "Photos")
        @JsonDefaultValueList
        val photos: List<Photo>

) : Parcelable
