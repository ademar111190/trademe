package ademar.study.trademe.core.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import se.ansman.kotshi.JsonDefaultValueString
import se.ansman.kotshi.JsonSerializable

@JsonSerializable
@Parcelize
data class PhotoValue(

        @Json(name = "Thumbnail")
        @JsonDefaultValueString(value = "")
        val thumbnail: String,

        @Json(name = "List")
        @JsonDefaultValueString(value = "")
        val list: String,

        @Json(name = "Medium")
        @JsonDefaultValueString(value = "")
        val medium: String,

        @Json(name = "Gallery")
        @JsonDefaultValueString(value = "")
        val gallery: String,

        @Json(name = "Large")
        @JsonDefaultValueString(value = "")
        val large: String,

        @Json(name = "FullSize")
        @JsonDefaultValueString(value = "")
        val fullSize: String

) : Parcelable
