package ademar.study.trademe.core.model

import ademar.study.trademe.core.injection.JsonDefaultValueList
import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import se.ansman.kotshi.JsonSerializable

@JsonSerializable
@Parcelize
data class Search(

        @Json(name = "List")
        @JsonDefaultValueList
        val result: List<Listing>

) : Parcelable
