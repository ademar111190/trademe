package ademar.study.trademe.core.repository.datasource

import ademar.study.trademe.core.model.ListingDetail
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ListingCloudRepository {

    @GET("/v1/Listings/{listingId}.json")
    fun getListingDetail(
            @Path("listingId") listingId: Long,
            @Query("increment_view_count") incrementViewCount: Boolean = false,
            @Query("return_member_profile") returnMemberProfile: Boolean = false
    ): Observable<ListingDetail>

}
