package ademar.study.trademe.core.repository.datasource

import ademar.study.trademe.core.model.Category
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CategoryMemoryRepository @Inject constructor() {

    var categories: HashMap<String, Category> = hashMapOf()

}
