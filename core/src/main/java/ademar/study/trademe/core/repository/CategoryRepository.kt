package ademar.study.trademe.core.repository

import ademar.study.trademe.core.model.Category
import ademar.study.trademe.core.repository.datasource.CategoryCloudRepository
import ademar.study.trademe.core.repository.datasource.CategoryMemoryRepository
import io.reactivex.Observable
import javax.inject.Inject

class CategoryRepository @Inject constructor(

        private val cloud: CategoryCloudRepository,
        private val memory: CategoryMemoryRepository
        // TODO create the persistence repository

) {

    fun getCategory(number: String): Observable<Category> {
        val cached = memory.categories[number]
        val fromCloud = cloud.getCategory(number)
                .doOnNext(::cacheCategory)
        return if (cached == null) {
            fromCloud
        } else {
            Observable.fromIterable(listOf(cached)).mergeWith(fromCloud
                    .onErrorResumeNext(Observable.empty())
                    .filter { cached != it })
        }
    }

    private fun cacheCategory(category: Category) {
        memory.categories[category.number] = category
        category.subCategories.forEach(::cacheCategory)
    }

}
