package ademar.study.trademe.core.interactor

import ademar.study.trademe.core.model.Category
import ademar.study.trademe.core.repository.SearchRepository
import io.reactivex.Observable
import javax.inject.Inject

class SearchListing @Inject constructor(

        private val repository: SearchRepository

) {

    fun execute(query: String? = null, category: Category? = null) = when {
        query != null && category != null -> repository.searchQueryInCategory(query, category)
        query != null -> repository.searchQuery(query)
        category != null -> repository.searchCategory(category)
        else -> Observable.empty()
    }

}
