package ademar.study.trademe.core.repository

import ademar.study.trademe.core.model.Category
import ademar.study.trademe.core.model.Listing
import ademar.study.trademe.core.repository.datasource.SearchCloudRepository
import io.reactivex.Observable
import javax.inject.Inject

class SearchRepository @Inject constructor(

        private val cloud: SearchCloudRepository

) {

    fun searchCategory(category: Category): Observable<Listing> = cloud
            .search(categoryNumber = category.number)
            .flatMapIterable { it.result }

    fun searchQuery(query: String): Observable<Listing> = cloud
            .search(query = query)
            .flatMapIterable { it.result }

    fun searchQueryInCategory(query: String, category: Category): Observable<Listing> = cloud
            .search(categoryNumber = category.number, query = query)
            .flatMapIterable { it.result }

}
