package ademar.study.trademe.core.repository

import ademar.study.trademe.core.model.ListingDetail
import ademar.study.trademe.core.repository.datasource.ListingCloudRepository
import ademar.study.trademe.core.repository.datasource.ListingMemoryRepository
import io.reactivex.Observable
import javax.inject.Inject

class ListingRepository @Inject constructor(

        private val cloud: ListingCloudRepository,
        private val memory: ListingMemoryRepository
        // TODO create the persistence repository

) {

    fun getListingDetail(id: Long): Observable<ListingDetail> {
        val cached = memory.listings[id]
        val fromCloud = cloud.getListingDetail(id)
                .doOnNext(::cacheListing)
        return if (cached == null) {
            fromCloud
        } else {
            Observable.fromIterable(listOf(cached)).mergeWith(fromCloud
                    .onErrorResumeNext(Observable.empty())
                    .filter { cached != it })
        }
    }

    private fun cacheListing(listing: ListingDetail) {
        memory.listings[listing.id] = listing
    }

}
