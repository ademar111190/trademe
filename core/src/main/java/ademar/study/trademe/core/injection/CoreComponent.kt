package ademar.study.trademe.core.injection

import ademar.study.trademe.core.repository.datasource.*
import android.content.Context
import dagger.Component
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import javax.inject.Singleton

@Singleton
@Component(modules = [CoreModule::class])
interface CoreComponent {

    val context: Context
    val httpLoggingInterceptor: HttpLoggingInterceptor
    val okHttpClient: OkHttpClient
    val retrofit: Retrofit

    val categoryCloudRepository: CategoryCloudRepository
    val categoryMemoryRepository: CategoryMemoryRepository

    val listingCloudRepository: ListingCloudRepository
    val listingMemoryRepository: ListingMemoryRepository

    val searchCloudRepository: SearchCloudRepository

}
