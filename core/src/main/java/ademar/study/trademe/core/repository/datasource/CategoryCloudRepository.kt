package ademar.study.trademe.core.repository.datasource

import ademar.study.trademe.core.model.Category
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface CategoryCloudRepository {

    @GET("/v1/Categories/{number}.json")
    fun getCategory(
            @Path("number") number: String,
            @Query("with_counts") with_counts: Boolean = true,
            @Query("depth") depth: Int = 1
    ): Observable<Category>

}
