package ademar.study.trademe.core.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import se.ansman.kotshi.JsonSerializable

@JsonSerializable
@Parcelize
data class Photo(

        @Json(name = "Value")
        val value: PhotoValue

) : Parcelable
