# TradeMe [![Build Status](https://www.bitrise.io/app/0c1a5f33eed91912/status.svg?token=t7AiY_-NY6JaHXI0YlRHhw)](https://www.bitrise.io/app/0c1a5f33eed91912)

You can run the tests with:

```
$ ./gradlew clean \
core:testDebugUnitTest \
app:testDevelopmentDebugUnitTest
```